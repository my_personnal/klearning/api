From openjdk:17
EXPOSE 8080
ADD klearning.jar klearning.jar
ENTRYPOINT ["java","-jar","klearning.jar"]
