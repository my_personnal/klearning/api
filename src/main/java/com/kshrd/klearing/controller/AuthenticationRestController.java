package com.kshrd.klearing.controller;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.configuration.Jwt.TokenUtils;
import com.kshrd.klearing.model.request.EGender;
import com.kshrd.klearing.model.request.ERoles;
import com.kshrd.klearing.model.request.JwtRequestModel;
import com.kshrd.klearing.model.request.UserRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.response.JwtResponseModel;
import com.kshrd.klearing.model.response.UserResponse;
import com.kshrd.klearing.repository.UserRepository;
import com.kshrd.klearing.service.AuthenticationUserService;
import com.kshrd.klearing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${baseUrl}/authentication")
@CrossOrigin(origins = "*",allowedHeaders = "*")
public class AuthenticationRestController {
    @Autowired
    private TokenUtils tokenUtils;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userServices;

    @Autowired
    private AuthenticationUserService userService;

    public static int userId;
    Collection<? extends GrantedAuthority> role;

    public AuthenticationRestController(AuthenticationUserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequestModel request)throws Exception, DataAccessException {
        Boolean st = userService.userStatus(request.getUsername().trim());
        List<String> userName=userService.getUsername();
        System.out.println(userName.toString());
        Boolean state=false;
        for(int i=0;i<userName.size();i++){
            if(userName.get(i).equalsIgnoreCase(request.getUsername().trim())) {
                state=true;
            }
        }
        if(state==true){
            if (st == false) {
                return ResponseEntity.ok(new JwtResponseModel("This Username has been deleted","No Token found"));
            }
            else {
                try {
                    System.out.println("username: "+request.getUsername()+", pass:"+request.getPassword());
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
                    System.out.println("Hello Auth");
                } catch (DataAccessException ex) {
                    throw new Exception("Error Connecting", ex);
                } catch (BadCredentialsException e) {
                    throw new Exception("Incorrect username or password", e);
                }
                final UserDetails userDetails = userService.loadUserByUsername(request.getUsername());
                int id = userService.selectUserID(request.getUsername());
                final String token = tokenUtils.generateToken(userDetails);
                System.out.println("Name: "+request.getUsername() + "Password: "+request.getPassword() + "Token: "+token);
                userId = id;
                role = userDetails.getAuthorities();
                return ResponseEntity.ok(new JwtResponseModel("get token success", token, userDetails.getAuthorities()));
            }

        }
        else {
            return ResponseEntity.ok(new JwtResponseModel("Username doesn't match any account" ,"No Token for this account"));
        }
    }

    @PostMapping("/register")
    public ResponseEntity<APIResponse<UserResponse>> register(@Valid @RequestBody UserRequest userRequest,
                                                                @RequestParam(value = "gender", defaultValue = "Male") EGender gender,
                                                                @RequestParam(value = "roles", defaultValue = "User") ERoles roles) {
        BaseMessage.obj = "User";
        userRequest.setGender(gender);
        if(roles.name() == "Admin"){
            userRequest.setRole_id(21);
        }
        if(roles.name() == "User"){
            userRequest.setRole_id(1);
        }
        APIResponse<UserResponse> response = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        java.time.LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        userRequest.setRegisterDate(offsetTime);
        List<String> usernames = userRepository.getAllUsername();
        List<String> emails = userRepository.getAllEmail();
        boolean u = false;
        boolean e = false;
        for(int i=0;i<usernames.size();i++){
            if(usernames.get(i).equalsIgnoreCase(userRequest.getUsername())){
                System.out.println(usernames.get(i));
                u = true;
                System.out.println("username is duplicate");
                //Message Duplicate Username
            }
        }
        for(int i=0;i<emails.size();i++){
            if(emails.get(i).equalsIgnoreCase(userRequest.getEmail())){
                e = true;
                System.out.println("email is duplicate");
                //Message Duplicate Username
            }
        }
        if((u == true) && (e == true)){
            response.setMessage("Username and Email already exist");
            //Message Duplicate Username and Email
            response.setStatus(HttpStatus.BAD_REQUEST);

        }else if((u == true) && ( e == false)){
            //Message Duplicate Username and Email
            response.setMessage("Username already exist");
            response.setStatus(HttpStatus.BAD_REQUEST);

        }
        else if( (u == false ) &&( e == true)){
            //Message Duplicate Username and Email
            response.setMessage("Email already exist");
            response.setStatus(HttpStatus.BAD_REQUEST);
        }else {
            UserResponse responseUser = userServices.insertUser(userRequest);
            System.out.println(responseUser);
            response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
            response.setData(responseUser);
            response.setStatus(HttpStatus.OK);

        }
        System.out.println("status:"+u+e);
        return ResponseEntity.ok(response);

    }
}
