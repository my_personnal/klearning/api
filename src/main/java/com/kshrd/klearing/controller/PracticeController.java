package com.kshrd.klearing.controller;


import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.materials.*;
import com.kshrd.klearing.model.materials.content.practice.PracticeContent;
import com.kshrd.klearing.model.materials.content.practice.PracticeFillingTheBlank;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.repository.PracticeRepository;
import com.kshrd.klearing.service.MaterialService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;


@RestController
@RequestMapping("/${baseUrl}/practice/")
public class PracticeController {

    @Autowired
    MaterialService materialService;

    @Autowired
    PracticeRepository practiceRepository;

    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "Practice";
    }


    @GetMapping("/getPracticeContentByTypeIDLevelIdAndSubTypeId")
    public ResponseEntity<APIResponse<List<PracticeContent>>> getPracticeContentByTypeIDLevelIdAndSubTypeId(Integer levelId, Integer subtypeId, Integer typeId) throws NotFoundException {
        BaseMessage.obj = "Get Practice";
        APIResponse<List<PracticeContent>> response = new APIResponse<>();
        List<PracticeContent> userResponseList = materialService.getPracticeContentByTypeIDLevelIdAndSubTypeId(levelId,subtypeId,typeId);
        if(userResponseList.size() == 0){
            throw new NotFoundException("Not Found");
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(userResponseList);
            System.out.println(userResponseList);
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("/getPracticeFillingTheBlankContentByTypeIDLevelIdAndSubTypeId")
    public ResponseEntity<APIResponse<List<PracticeFillingTheBlank>>> getPracticeFillingTheBlankContentByTypeIDLevelIdAndSubTypeId(Integer levelId, Integer subtypeId, Integer typeId) throws NotFoundException {
        BaseMessage.obj = "Get Practice filling the blank";
        APIResponse<List<PracticeFillingTheBlank>> response = new APIResponse<>();
        List<PracticeFillingTheBlank> userResponseList = materialService.getPracticeFillingTheBlankContentByTypeIDLevelIdAndSubTypeId(levelId,subtypeId,typeId);
        if(userResponseList.size() == 0){
            throw new NotFoundException("Not Found");
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(userResponseList);
            System.out.println(userResponseList);
        }
            return ResponseEntity.ok(response);
        }

    @PostMapping("/InsertPracticeContent")
    public ResponseEntity<APIResponse<MTPracticeResponse>> InsertPracticeContent(@Valid @RequestBody MTPracticeRequest mtPracticeRequest) {
        BaseMessage.obj ="Practice Content";
        APIResponse<MTPracticeResponse> responseAPI = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        mtPracticeRequest.setCreatedDate(offsetTime);
        mtPracticeRequest.setUserId(AuthenticationRestController.userId);
        mtPracticeRequest.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;
        boolean materialName = false;

//        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(mtPracticeRequest.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {

            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == mtPracticeRequest.getLevelID()) {
                    levelsID = true;
                }
            }
            for (int i = 0; i < subtype.size(); i++) {
                if (subtype.get(i) == mtPracticeRequest.getSubTypeID()) {
                    subtypeID = true;
                }
            }
            for (int i = 0; i < type.size(); i++) {
                if (type.get(i) == mtPracticeRequest.getTypeID()) {
                    typeID = true;
                }
            }
            if (levelsID == true && subtypeID == true && typeID == true) {
                System.out.println("from all true");
                MTPracticeResponse content = materialService.insertPracticeContent(mtPracticeRequest);
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                responseAPI.setData(content);
            } else if (levelsID == true && subtypeID == true && typeID == false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            } else if (levelsID == true && subtypeID == false && typeID == true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == true && subtypeID == false && typeID == false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
        }
        return ResponseEntity.ok(responseAPI);
        }
    @PostMapping("/InsertPracticeFillingTheBlankContent")
    public ResponseEntity<APIResponse<MTPracticeFillingTheBlankResponse>> InsertPracticeFillingTheBlankContent(@Valid @RequestBody MTPracticeFillingTheBlankRequest mtPracticeFillingTheBlankRequest) {
        BaseMessage.obj ="Filling the Blank Content";
        APIResponse<MTPracticeFillingTheBlankResponse> responseAPI = new APIResponse<>();
        //set timezone in local
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        mtPracticeFillingTheBlankRequest.setCreatedDate(offsetTime);
        //set token for user login
        mtPracticeFillingTheBlankRequest.setUserId(AuthenticationRestController.userId);
        mtPracticeFillingTheBlankRequest.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean materialName = false;
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;

//        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(mtPracticeFillingTheBlankRequest.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {
            System.out.println("material:"+materialName);
            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == mtPracticeFillingTheBlankRequest.getLevelID()) {
                    levelsID = true;
                }
            }
            for(int i=0;i<subtype.size(); i++){
                if(subtype.get(i) == mtPracticeFillingTheBlankRequest.getSubTypeID()){
                    subtypeID = true;
                }
            }
            for(int i=0 ; i<type.size(); i++){
                if(type.get(i) == mtPracticeFillingTheBlankRequest.getTypeID()){
                    typeID = true;
                }
            }
            if (levelsID ==true && subtypeID == true && typeID ==true) {
                MTPracticeFillingTheBlankResponse content = materialService.insertPracticeFillingTheBlankContent(mtPracticeFillingTheBlankRequest);
                System.out.println(content);
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                responseAPI.setData(content);
            }else if (levelsID ==true && subtypeID == true && typeID ==false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            }else if (levelsID ==true && subtypeID == false && typeID ==true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
            else if (levelsID ==false && subtypeID == true && typeID ==true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
            else if (levelsID ==false && subtypeID == true && typeID ==false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
            else if (levelsID ==true && subtypeID == false && typeID ==false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
            else if (levelsID ==false && subtypeID == false && typeID ==true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
            else if (levelsID ==false && subtypeID == false && typeID ==false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
            else
            {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            }
        }
        return ResponseEntity.ok(responseAPI);
    }

    @PutMapping("/UpdatePracticeFillingTheBlankContentByID")
    public ResponseEntity<APIResponse<MTPracticeFillingTheBlankUpdateRequest>> UpdatePracticeFillingTheBlankContentByID (@RequestBody MTPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest)throws NotFoundException {
        BaseMessage.obj ="Update Practice filling the blank by ID";
        APIResponse<MTPracticeFillingTheBlankUpdateRequest> responseAPI = new APIResponse<>();

        MTPracticeFillingTheBlankUpdateRequest updatePractice = materialService.mtPracticeFillingTheBlankUpdateRequest(mtPracticeFillingTheBlankUpdateRequest);
        if(updatePractice == null){
            throw new NotFoundException("Can not find with id: "+mtPracticeFillingTheBlankUpdateRequest.getId());
        }else{
            responseAPI.setStatus(HttpStatus.OK);
            responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            responseAPI.setData(updatePractice);
        }

        return ResponseEntity.ok(responseAPI);
    }

    @PutMapping("/UpdateAllPracticeFillingTheBlankContentByID")
    public ResponseEntity<APIResponse<MTAllPracticeFillingTheBlankUpdateResponse>> UpdateAllPracticeFillingTheBlankContentByID (@RequestBody MTAllPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest)throws NotFoundException {
        BaseMessage.obj ="Update All Practice filling the blank by ID";
        APIResponse<MTAllPracticeFillingTheBlankUpdateResponse> responseAPI = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        mtPracticeFillingTheBlankUpdateRequest.setCreatedDate(offsetTime);
        mtPracticeFillingTheBlankUpdateRequest.setUserId(AuthenticationRestController.userId);
        mtPracticeFillingTheBlankUpdateRequest.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;
        boolean materialName = false;
///        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(mtPracticeFillingTheBlankUpdateRequest.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {

            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == mtPracticeFillingTheBlankUpdateRequest.getLevelID()) {
                    levelsID = true;
                }
            }
            for (int i = 0; i < subtype.size(); i++) {
                if (subtype.get(i) == mtPracticeFillingTheBlankUpdateRequest.getSubTypeID()) {
                    subtypeID = true;
                }
            }
            for (int i = 0; i < type.size(); i++) {
                if (type.get(i) == mtPracticeFillingTheBlankUpdateRequest.getTypeID()) {
                    typeID = true;
                }
            }
            if (levelsID == true && subtypeID == true && typeID == true) {

                System.out.println("from all true");
                MTAllPracticeFillingTheBlankUpdateResponse updatePractice = materialService.mtAllPracticeFillingTheBlankUpdateRequest(mtPracticeFillingTheBlankUpdateRequest);
                if(updatePractice == null) {
                    throw new NotFoundException("Can not find id: " + mtPracticeFillingTheBlankUpdateRequest.getId());
                }
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                responseAPI.setData(updatePractice);
            } else if (levelsID == true && subtypeID == true && typeID == false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            } else if (levelsID == true && subtypeID == false && typeID == true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == true && subtypeID == false && typeID == false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
        }
        return ResponseEntity.ok(responseAPI);
    }


    @PutMapping("/UpdateAllPracticeContentByID")
    public ResponseEntity<APIResponse<MTAllPracticeFillingTheBlankUpdateResponse>> UpdateAllPracticeContentByID (@RequestBody MTAllPracticeContentUpdateRequest mtAllPracticeContentUpdateRequest)throws NotFoundException {
        BaseMessage.obj ="Update Practice by ID";
        APIResponse<MTAllPracticeFillingTheBlankUpdateResponse> responseAPI = new APIResponse<>();

        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        mtAllPracticeContentUpdateRequest.setCreatedDate(offsetTime);
        mtAllPracticeContentUpdateRequest.setUserId(AuthenticationRestController.userId);
        mtAllPracticeContentUpdateRequest.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;
        boolean materialName = false;
///        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(mtAllPracticeContentUpdateRequest.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {

            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == mtAllPracticeContentUpdateRequest.getLevelID()) {
                    levelsID = true;
                }
            }
            for (int i = 0; i < subtype.size(); i++) {
                if (subtype.get(i) == mtAllPracticeContentUpdateRequest.getSubTypeID()) {
                    subtypeID = true;
                }
            }
            for (int i = 0; i < type.size(); i++) {
                if (type.get(i) == mtAllPracticeContentUpdateRequest.getTypeID()) {
                    typeID = true;
                }
            }
            if (levelsID == true && subtypeID == true && typeID == true) {

                MTAllPracticeFillingTheBlankUpdateResponse updatePractice = materialService.UpdateAllPracticeContentByID(mtAllPracticeContentUpdateRequest);
                if(updatePractice == null){
                    throw new NotFoundException("Can not find id: "+mtAllPracticeContentUpdateRequest.getId());
                }
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                responseAPI.setData(updatePractice);
            } else if (levelsID == true && subtypeID == true && typeID == false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            } else if (levelsID == true && subtypeID == false && typeID == true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == true && subtypeID == false && typeID == false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }

            }
        return ResponseEntity.ok(responseAPI);
    }

    @PutMapping("/UpdatePracticeContentByID")
    public ResponseEntity<APIResponse<MTPracticeContentUpdateRequest>> UpdatePracticeContentByID (@RequestBody MTPracticeContentUpdateRequest mtPracticeContentUpdateRequest)throws NotFoundException {
        BaseMessage.obj ="Update Practice by ID";
        APIResponse<MTPracticeContentUpdateRequest> responseAPI = new APIResponse<>();
        MTPracticeContentUpdateRequest updatePractice = materialService.UpdatePracticeContentByID(mtPracticeContentUpdateRequest);
        if(updatePractice == null){
            throw new NotFoundException("Can not find id: "+mtPracticeContentUpdateRequest.getId());
        }else {
            responseAPI.setStatus(HttpStatus.OK);
            responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            responseAPI.setData(updatePractice);
        }
        return ResponseEntity.ok(responseAPI);
    }



}
