package com.kshrd.klearing.controller;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.materials.*;
import com.kshrd.klearing.model.materials.content.Content;
import com.kshrd.klearing.model.materials.content.SubType;
import com.kshrd.klearing.model.request.UpdateAllMaterialRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.repository.PracticeRepository;
import com.kshrd.klearing.service.MaterialService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

@RestController
@RequestMapping("/${baseUrl}/material/")
public class MaterialController {

    @Autowired
    MaterialService materialService;

    @Autowired
    PracticeRepository practiceRepository;

    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "Material";
    }

    @PostMapping("/InsertMaterial")
    public ResponseEntity<APIResponse<MaterialResponse>> InsertMaterial(@Valid @RequestBody  MaterialRequest materialRequest) {
        BaseMessage.obj ="Materials";
        APIResponse<MaterialResponse> responseAPI = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        materialRequest.setCreatedDate(offsetTime);
        materialRequest.setUserId(AuthenticationRestController.userId);
        materialRequest.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;
        boolean materialName = false;
///        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(materialRequest.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {

            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == materialRequest.getLevelID()) {
                    levelsID = true;
                }
            }
            for (int i = 0; i < subtype.size(); i++) {
                if (subtype.get(i) == materialRequest.getSubTypeID()) {
                    subtypeID = true;
                }
            }
            for (int i = 0; i < type.size(); i++) {
                if (type.get(i) == materialRequest.getTypeID()) {
                    typeID = true;
                }
            }
            if (levelsID == true && subtypeID == true && typeID == true) {
                MaterialResponse content = materialService.InsertMaterial(materialRequest);
                System.out.println(materialRequest.getName());
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                responseAPI.setData(content);
            } else if (levelsID == true && subtypeID == true && typeID == false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            } else if (levelsID == true && subtypeID == false && typeID == true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == true && subtypeID == false && typeID == false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
        }
        return ResponseEntity.ok(responseAPI);
    }

    @PostMapping("/InsertVocabForConsonant")
    public ResponseEntity<APIResponse<MTVocabForConsonantResponse>> InsertVocabForConsonant(@RequestBody MTVocabForConsonantRequest mtVocabForConsonantRequest) {
        BaseMessage.obj ="Material";
        APIResponse<MTVocabForConsonantResponse> responseAPI = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        mtVocabForConsonantRequest.setCreatedDate(offsetTime);
        mtVocabForConsonantRequest.setUserID(AuthenticationRestController.userId);
        mtVocabForConsonantRequest.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;
        boolean materialName = false;
///        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(mtVocabForConsonantRequest.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {

            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == mtVocabForConsonantRequest.getLevelID()) {
                    levelsID = true;
                }
            }
            for (int i = 0; i < subtype.size(); i++) {
                if (subtype.get(i) == mtVocabForConsonantRequest.getSubTypeID()) {
                    subtypeID = true;
                }
            }
            for (int i = 0; i < type.size(); i++) {
                if (type.get(i) == mtVocabForConsonantRequest.getTypeID()) {
                    typeID = true;
                }
            }
            if (levelsID == true && subtypeID == true && typeID == true) {
                MTVocabForConsonantResponse content = materialService.InsertMaterial(mtVocabForConsonantRequest);
                System.out.println(content);
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                responseAPI.setData(content);
            } else if (levelsID == true && subtypeID == true && typeID == false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            } else if (levelsID == true && subtypeID == false && typeID == true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == true && subtypeID == false && typeID == false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }

        }return ResponseEntity.ok(responseAPI);
    }

    @PutMapping("/UpdateContentByMaterialId")
    public ResponseEntity<APIResponse<MaterialUpdateRequest>> UpdateContentByMaterialId (@RequestBody MaterialUpdateRequest UpdateByMaterialId) {
        BaseMessage.obj ="Update Content By Material ID";
        APIResponse<MaterialUpdateRequest> responseAPI = new APIResponse<>();
        MaterialUpdateRequest insertMaterial = materialService.updateMaterialContent(UpdateByMaterialId);
        System.out.println(insertMaterial);
        if(insertMaterial == null){
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            responseAPI.setMessage(BaseMessage.Error.UPDATE_ERROR.getMessage());
            responseAPI.setData(insertMaterial);
        }else {
            responseAPI.setStatus(HttpStatus.OK);
            responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            responseAPI.setData(insertMaterial);
        }

        return ResponseEntity.ok(responseAPI);
    }

    @PutMapping("/UpdateMaterialById")
    public ResponseEntity<APIResponse<UpdateAllMaterialRequest>> UpdateMaterialById (@RequestBody UpdateAllMaterialRequest updateAllMaterialRequest) throws NotFoundException {
        BaseMessage.obj ="Update Material  By  ID";
        APIResponse<UpdateAllMaterialRequest> responseAPI = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        updateAllMaterialRequest.setCreatedDate(offsetTime);
        updateAllMaterialRequest.setUserId(AuthenticationRestController.userId);
        updateAllMaterialRequest.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;
        boolean materialName = false;
///        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(updateAllMaterialRequest.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {

            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == updateAllMaterialRequest.getLevelID()) {
                    levelsID = true;
                }
            }
            for (int i = 0; i < subtype.size(); i++) {
                if (subtype.get(i) == updateAllMaterialRequest.getSubTypeID()) {
                    subtypeID = true;
                }
            }
            for (int i = 0; i < type.size(); i++) {
                if (type.get(i) == updateAllMaterialRequest.getTypeID()) {
                    typeID = true;
                }
            }
            if (levelsID == true && subtypeID == true && typeID == true) {
                UpdateAllMaterialRequest updateMaterial = materialService.UpdateMaterialById(updateAllMaterialRequest);
                if (updateMaterial == null) {
                    throw new NotFoundException("ID Not found");
                }
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                responseAPI.setData(updateMaterial);
            } else if (levelsID == true && subtypeID == true && typeID == false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            } else if (levelsID == true && subtypeID == false && typeID == true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == true && subtypeID == false && typeID == false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }

        }
        return ResponseEntity.ok(responseAPI);
    }

    @GetMapping("/getSubTypeByLevelIdAndTypeId")
    public ResponseEntity<APIResponse<List<SubType>>> getSubTypeByLevelIdAndTypeId(Integer levelId, Integer typeId)throws NotFoundException {
        APIResponse<List<SubType>> response = new APIResponse<>();
        List<SubType> userResponseList = materialService.getSubTypeByLevelIdAndTypeId(levelId,typeId);
        if(userResponseList.size() == 0){
            throw new NotFoundException("Not found");
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(userResponseList);
        }

        return ResponseEntity.ok(response);
    }

    @GetMapping("/getContentByTypeIDLevelIdAndSubTypeId")
    public ResponseEntity<APIResponse<List<Content>>> getContentByTypeIDLevelIdAndSubTypeId(Integer levelId,Integer typeId, Integer subtypeId) throws NotFoundException {
        APIResponse<List<Content>> response = new APIResponse<>();
        List<Content> userResponseList = materialService.getContentByTypeIDLevelIdAndSubTypeId(levelId,typeId,subtypeId);
        if(userResponseList.size() == 0){
            throw new NotFoundException("Not found...");
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(userResponseList);
        }
        return ResponseEntity.ok(response);
    }


    @GetMapping("/getMaterialBySubTypeIDLevelIDAndTypeID")
    public ResponseEntity<APIResponse<List<MaterialResponse>>> getMaterialBySubTypeIDLevelIDAndTypeID(Integer levelID,Integer subtypeID, Integer typeID) throws NotFoundException {
        BaseMessage.obj = "Get Material";
        APIResponse<List<MaterialResponse>> response = new APIResponse<>();
        List<MaterialResponse> materialResponseList = materialService.getAllMaterials(levelID, subtypeID, typeID);
        if(materialResponseList.size() == 0){
            throw new NotFoundException("Not found...");
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(materialResponseList);
        }
        return ResponseEntity.ok(response);
    }
    @PostMapping("InsertHeaderOfMaterialType")
    public ResponseEntity<APIResponse<HeaderOfMaterialTypeResponse>> InsertHeaderOfMaterialType(@Valid @RequestBody HeaderOfMaterialTypeRequest materialRequest) {
        BaseMessage.obj ="Header Of Material Type";
        APIResponse<HeaderOfMaterialTypeResponse> responseAPI = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        materialRequest.setCreatedDate(offsetTime);
        materialRequest.setUserID(AuthenticationRestController.userId);
        materialRequest.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;
        boolean materialName = false;
///        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(materialRequest.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {

            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == materialRequest.getLevelID()) {
                    levelsID = true;
                }
            }
            for (int i = 0; i < subtype.size(); i++) {
                if (subtype.get(i) == materialRequest.getSubTypeID()) {
                    subtypeID = true;
                }
            }
            for (int i = 0; i < type.size(); i++) {
                if (type.get(i) == materialRequest.getTypeID()) {
                    typeID = true;
                }
            }
            if (levelsID == true && subtypeID == true && typeID == true) {
                HeaderOfMaterialTypeResponse content = materialService.InsertHeaderOfMaterialType(materialRequest);
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                responseAPI.setData(content);
            } else if (levelsID == true && subtypeID == true && typeID == false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            } else if (levelsID == true && subtypeID == false && typeID == true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == true && subtypeID == false && typeID == false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }
        }
        return ResponseEntity.ok(responseAPI);
    }

    @PutMapping("/updateAllVocabForConsonantById")
    public ResponseEntity<APIResponse<MTVocabForConsonantResponse>> updateAllVocabForConsonantById  (@RequestBody UpdateAllMTVocabForConsonantRequest updateVocabForConsonantById) throws NotFoundException {
        BaseMessage.obj ="Update Content By Material ID";
        APIResponse<MTVocabForConsonantResponse> responseAPI = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        updateVocabForConsonantById.setCreatedDate(offsetTime);
        updateVocabForConsonantById.setUserId(AuthenticationRestController.userId);
        updateVocabForConsonantById.setStatus(0);
        List<Integer> level = practiceRepository.getAllLevel();
        List<Integer> subtype = practiceRepository.getAllSubTypeID();
        List<Integer> type = practiceRepository.getAllTypeID();
        List<String> nameMaterial = practiceRepository.getAllNameMaterial();
        boolean levelsID = false;
        boolean subtypeID = false;
        boolean typeID = false;
        boolean materialName = false;
///        select material name
        for (int i = 0; i < nameMaterial.size(); i++) {
            if (nameMaterial.get(i).equalsIgnoreCase(updateVocabForConsonantById.getName())) {
                materialName = true;
            }
        }
        if(materialName){
            responseAPI.setMessage("name already exist.....");
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            System.out.println("material:"+materialName);
        }
        else {

            for (int i = 0; i < level.size(); i++) {
                if (level.get(i) == updateVocabForConsonantById.getLevelID()) {
                    levelsID = true;
                }
            }
            for (int i = 0; i < subtype.size(); i++) {
                if (subtype.get(i) == updateVocabForConsonantById.getSubTypeID()) {
                    subtypeID = true;
                }
            }
            for (int i = 0; i < type.size(); i++) {
                if (type.get(i) == updateVocabForConsonantById.getTypeID()) {
                    typeID = true;
                }
            }
            if (levelsID == true && subtypeID == true && typeID == true) {
                MTVocabForConsonantResponse updateVocabCon = materialService.updateAllVocabForConsonantById(updateVocabForConsonantById);
                if(updateVocabCon == null){
                    throw new NotFoundException("ID Not found");
                }
                responseAPI.setStatus(HttpStatus.OK);
                responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                responseAPI.setData(updateVocabCon);
            } else if (levelsID == true && subtypeID == true && typeID == false) {
                responseAPI.setMessage("TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);

            } else if (levelsID == true && subtypeID == false && typeID == true) {
                responseAPI.setMessage("SubtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == true) {
                responseAPI.setMessage("levels does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == true && typeID == false) {
                responseAPI.setMessage("Levels and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == true && subtypeID == false && typeID == false) {
                responseAPI.setMessage("subtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == true) {
                responseAPI.setMessage("levels and subtypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else if (levelsID == false && subtypeID == false && typeID == false) {
                responseAPI.setMessage("LevelID,SubTypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            } else {
                responseAPI.setMessage("LevelID,SubtypeID and TypeID does not exist...");
                responseAPI.setStatus(HttpStatus.NOT_FOUND);
            }

        }
        return ResponseEntity.ok(responseAPI);
    }

    @PutMapping("/updateVocabForConsonantById")
    public ResponseEntity<APIResponse<UpdateMTVocabForConsonantResponse>> updateVocabForConsonantById  (@RequestBody UpdateMTVocabForConsonantRequest updateVocabForConsonantById)throws NotFoundException {
        BaseMessage.obj ="Update Vocab for consonant By ID";
        APIResponse<UpdateMTVocabForConsonantResponse> responseAPI = new APIResponse<>();
        UpdateMTVocabForConsonantResponse updateVocabCon = materialService.updateVocabForConsonantById(updateVocabForConsonantById);
        if(updateVocabCon == null){
            responseAPI.setStatus(HttpStatus.NOT_FOUND);
            responseAPI.setMessage(BaseMessage.Error.UPDATE_ERROR.getMessage());
            responseAPI.setData(updateVocabCon);
        }else {
            responseAPI.setStatus(HttpStatus.OK);
            responseAPI.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            responseAPI.setData(updateVocabCon);
        }
        return ResponseEntity.ok(responseAPI);
    }

    @GetMapping("/getMaterialInfoByTypeIDAndLevelID")
    public ResponseEntity<APIResponse<List<MaterialInfoTypeResponse>>> getMaterialInfoByTypeIDAndLevelID(Integer levelID, Integer typeID) throws NotFoundException{
        BaseMessage.obj = "Get Material Information";
        APIResponse<List<MaterialInfoTypeResponse>> response = new APIResponse<>();
        List<MaterialInfoTypeResponse> mtVocabForConsonantResponses = materialService.getMaterialInfo(levelID,typeID);
        if(mtVocabForConsonantResponses.size() == 0){
            throw new NotFoundException("Not found...");
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(mtVocabForConsonantResponses);
        }
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/deleteMaterialById")
    public ResponseEntity<APIResponse<DeleteResponse>> deleteMaterialById(@RequestParam Integer deleteMaterialById)  {
        BaseMessage.obj = "Material";
        APIResponse<DeleteResponse> response = new APIResponse<>();
        DeleteResponse materialResponse = materialService.deleteMaterialById(deleteMaterialById);
        if(materialResponse == null){
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(materialResponse);
            response.setMessage(BaseMessage.Error.DELETE_ERROR.getMessage());
        }else {
            response.setStatus(HttpStatus.OK);
            response.setData(materialResponse);
            response.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());
        }
        return ResponseEntity.ok().body(response);
    }



}
