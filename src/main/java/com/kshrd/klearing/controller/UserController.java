package com.kshrd.klearing.controller;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.request.EGender;
import com.kshrd.klearing.model.request.ERoles;
import com.kshrd.klearing.model.request.UpdateUserRequest;
import com.kshrd.klearing.model.request.UserRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.response.UserResponse;
import com.kshrd.klearing.repository.RolesRepository;
import com.kshrd.klearing.repository.UserRepository;
import com.kshrd.klearing.service.UserService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

@RestController
@RequestMapping("${baseUrl}/user/")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    static RolesRepository rolesRepository;


    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "User";
    }

    @GetMapping("/getAllUser")
    public ResponseEntity<APIResponse<List<UserResponse>>> getAllUser() {
        BaseMessage.obj = "User";
        APIResponse<List<UserResponse>> response = new APIResponse<>();
        List<UserResponse> userResponseList = userService.getAllUsers();
        response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(userResponseList);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/searchUserByName")
    public ResponseEntity<APIResponse<List<UserResponse>>> searchUserByName(@RequestParam String name) throws NotFoundException {
        BaseMessage.obj = "User";
        APIResponse<List<UserResponse>> response = new APIResponse<>();
        List<UserResponse> rolesResponseList = userService.findUserByName(name);
        if(rolesResponseList.size() == 0){
            throw new NotFoundException("Can not find username "+name);
        }
        response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(rolesResponseList);

        return ResponseEntity.ok(response);
    }

    @PutMapping ("/updateUserById")
    public ResponseEntity<APIResponse<UserResponse>> updateUserById(@Valid @RequestBody UpdateUserRequest updateUserRequest,@RequestParam(value = "gender", defaultValue = "Male") EGender gender,@RequestParam(value = "roles", defaultValue = "User")ERoles roles)throws NotFoundException {
        BaseMessage.obj = "User";
        APIResponse<UserResponse> response = new APIResponse<>();
        updateUserRequest.setGender(gender);
        if(roles.name() == "Admin"){
            updateUserRequest.setRoleID(21);
        }
        if(roles.name() == "User"){
            updateUserRequest.setRoleID(1);
        }
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        java.time.LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        updateUserRequest.setRegisterDate(offsetTime);
        List<String> usernames = userRepository.getAllUsername();
        List<String> emails = userRepository.getAllEmail();
        boolean u = false;
        boolean e = false;
        for(int i=0;i<usernames.size();i++){
            if(usernames.get(i).equalsIgnoreCase(updateUserRequest.getUsername())){
                System.out.println(usernames.get(i));
                u = true;
                System.out.println("username is duplicate");
                //Message Duplicate Username
            }
        }
        for(int i=0;i<emails.size();i++){
            if(emails.get(i).equalsIgnoreCase(updateUserRequest.getEmail())){
                e = true;
                System.out.println("email is duplicate");
                //Message Duplicate Username
            }
        }
        if((u == true) && (e == true)){
            response.setMessage("Username and Email already exist");
            //Message Duplicate Username and Email
            response.setStatus(HttpStatus.BAD_REQUEST);

        }else if((u == true) && ( e == false)){
            //Message Duplicate Username and Email
            response.setMessage("Username already exist");
            response.setStatus(HttpStatus.BAD_REQUEST);

        }
        else if( (u == false ) &&( e == true)){
            //Message Duplicate Username and Email
            response.setMessage("Email already exist");
            response.setStatus(HttpStatus.BAD_REQUEST);
        }else {
            UserResponse updateUser = userService.updateUserById(updateUserRequest);
            if(updateUser == null){
                response.setMessage(BaseMessage.Error.UPDATE_ERROR.getMessage());
                response.setStatus(HttpStatus.NOT_FOUND);
            }else {
                response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                response.setData(updateUser);
                response.setStatus(HttpStatus.OK);
            }
        }

        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/deleteUserByStatus")
    public ResponseEntity<APIResponse<String>> deleteUserByStatus(@RequestParam Integer userId) throws NotFoundException {
        BaseMessage.obj = "User";
        APIResponse<String> response = new APIResponse<>();
        String userResponse = userService.deleteUserById(userId);
        if(userResponse == null){
            throw new NotFoundException("Unable find UserID "+userId);
        }
        response.setStatus(HttpStatus.OK);
        response.setData(userResponse);
        response.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());
        return ResponseEntity.ok().body(response);
    }



}
