package com.kshrd.klearing.controller;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.request.TypeRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.response.TypeResponse;
import com.kshrd.klearing.model.result.ResultRequest;
import com.kshrd.klearing.model.result.ResultResponse;
import com.kshrd.klearing.model.result.ResultUpdateRequest;
import com.kshrd.klearing.repository.ResultRepository;
import com.kshrd.klearing.service.ResultsService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/${baseUrl}/result/")
public class ResultController {

    @Autowired
    ResultsService resultsService;

    @Autowired
    ResultRepository resultRepository;

    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "Result";
    }

    @GetMapping("/getAllResults")
    public ResponseEntity<APIResponse<List<ResultResponse>>> getAllResults() throws NotFoundException {
        BaseMessage.obj = "Result";
        APIResponse<List<ResultResponse>> response = new APIResponse<>();
        List<ResultResponse> resultResponsesList = resultsService.findAllResults();
        if(resultResponsesList.size() == 0){
            throw new NotFoundException("Not Found All records");
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(resultResponsesList);
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("/searchResultBy")
    public ResponseEntity<APIResponse<List<ResultResponse>>> searchResultByID(@RequestParam Integer id) {
        APIResponse<List<ResultResponse>> response = new APIResponse<>();
        List<ResultResponse> typeResponse = resultsService.findLevelByName(id);
        if(typeResponse.size() == 0){
            response.setMessage("ID was not found...");
            response.setStatus(HttpStatus.NOT_FOUND);
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(typeResponse);
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/insertResult")
    public ResponseEntity<APIResponse<ResultResponse>> insertResult(@RequestBody ResultRequest resultRequest) {
        APIResponse<ResultResponse> response = new APIResponse<>();
        resultRequest.setUserID(AuthenticationRestController.userId);
        List<Integer> materialID = resultRepository.getAllMaterialID();
        boolean material = false;
        for(int i =0;i< materialID.size();i++){
            if(materialID.get(i) == resultRequest.getMaterialID()) {
                System.out.println("get from loop");
                material = true;
            }
        }
        if(material){
            if(resultRequest.getTotalScore() >= resultRequest.getUserScore()){
                ResultResponse resultResponse = resultsService.insertResult(resultRequest);
                response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                response.setStatus(HttpStatus.OK);
                response.setData(resultResponse);
            }
            else {
                response.setMessage("UserScore "+resultRequest.getUserScore()+" Can not be greater than of Total Score "+resultRequest.getTotalScore());
                response.setStatus(HttpStatus.NOT_FOUND);
                System.out.println("UserScore Can not be greater than of Total Score");
            }

        }else  {
            response.setMessage("Not found on Material ID: "+resultRequest.getMaterialID());
            response.setStatus(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(response);
    }

    @PutMapping("/updateResultById")
    public ResponseEntity<APIResponse<ResultResponse>> updateResultById(@RequestBody ResultUpdateRequest resultID) throws NotFoundException {
        APIResponse<ResultResponse> response = new APIResponse<>();
        resultID.setUserID(AuthenticationRestController.userId);
        List<Integer> materialID = resultRepository.getAllMaterialID();
        boolean material = false;
        for(int i =0;i< materialID.size();i++){
            if(materialID.get(i) == resultID.getMaterialID()) {
                material = true;
            }
        }
        if(material){
            if(resultID.getTotalScore() >= resultID.getUserScore()){
                ResultResponse resultResponse = resultsService.updateResultByID(resultID);
                if(resultResponse == null){
                    throw new NotFoundException("ID Not Found");
                }else {
                    response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                    response.setData(resultResponse);
                    response.setStatus(HttpStatus.OK);
                }
            }
            else {
                response.setMessage("UserScore "+resultID.getUserScore()+" Can not be greater than of Total Score "+resultID.getTotalScore());
                response.setStatus(HttpStatus.NOT_FOUND);
                System.out.println("UserScore Can not be greater than of Total Score");
            }

        }else  {
            response.setMessage("Not found on Material ID: "+resultID.getMaterialID());
            response.setStatus(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok().body(response);
    }
    @DeleteMapping("/deleteResultById")
    public ResponseEntity<APIResponse<ResultResponse>> deleteResultById(@RequestParam Integer id) {
        APIResponse<ResultResponse> response = new APIResponse<>();
        ResultResponse resultResponse = resultsService.deleteResultsByID(id);
        if(resultResponse == null){
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setData(resultResponse);
            response.setMessage("ID was not found...");
        }else {
            System.out.println(resultsService.deleteResultsByID(id));
            response.setStatus(HttpStatus.OK);
            response.setData(resultResponse);
            response.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());
        }
        return ResponseEntity.ok().body(response);
    }
}
