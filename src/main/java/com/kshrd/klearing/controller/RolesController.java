package com.kshrd.klearing.controller;

import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.request.EGender;
import com.kshrd.klearing.model.request.EVisibility;
import com.kshrd.klearing.model.request.RolesRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.response.RolesResponse;
import com.kshrd.klearing.repository.RolesRepository;
import com.kshrd.klearing.service.RolesService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.NotReadablePropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@Validated
@RequestMapping("${baseUrl}/role/")
public class RolesController {

    @Autowired
    private RolesService rolesService;

    @Autowired
    private RolesRepository rolesRepository;

    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "Roles";
    }

    @GetMapping("/getAllRoles")
    public ResponseEntity<APIResponse<List<RolesResponse>>> getAllRoles() {
        BaseMessage.obj ="Roles";
        APIResponse<List<RolesResponse>> response = new APIResponse<>();
        List<RolesResponse> rolesResponseList = rolesService.findAllRoles();
        response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(rolesResponseList);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/searchRolesByName")
    public ResponseEntity<APIResponse<List<RolesResponse>>> searchRolesByName(@RequestParam String name) throws NotFoundException {
        BaseMessage.obj ="Roles";
        APIResponse<List<RolesResponse>> response = new APIResponse<>();
        List<RolesResponse> rolesResponseList = rolesService.findRoleByName(name);
        if(rolesResponseList.size() == 0){
            throw new NotFoundException("Can not Find with name "+name);
        }else {
            response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(rolesResponseList);
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/insertRoles")
    public ResponseEntity<APIResponse<RolesResponse>> insertRoles(@Valid @RequestBody RolesRequest rolesRequest) {
        BaseMessage.obj ="Roles";
        APIResponse<RolesResponse> response = new APIResponse<>();
            List<String> roleName = rolesRepository.findNameAllRoles();
            boolean role = false;
            for (int i = 0; i < roleName.size(); i++) {
                if (roleName.get(i).equalsIgnoreCase(rolesRequest.getName())) {
                    role = true;
                }
            }
            if (role == true) {
                response.setMessage("Roles already exist");
                //Message Duplicate roleName
                response.setStatus(HttpStatus.BAD_REQUEST);
            } else {
                RolesResponse rolesResponse = rolesService.insertRoles(rolesRequest);
                response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                response.setData(rolesResponse);
                response.setStatus(HttpStatus.OK);

            }
        return ResponseEntity.ok(response);
    }
    @PutMapping ("/updateRoleById")
    public ResponseEntity<APIResponse<RolesResponse>> updateRoleById(@RequestBody RolesResponse roleName) throws NotFoundException {
        APIResponse<RolesResponse> response = new APIResponse<>();
        List<String> allRoleName = rolesRepository.findNameAllRoles();
        RolesResponse rolesResponse = rolesService.updateRolesByName(roleName);
        if (rolesResponse == null) {
            throw new NotFoundException("Can not Find with roleName " + roleName);
        }
        boolean role = false;
        for (int i = 0; i < allRoleName.size(); i++) {
            if (allRoleName.get(i).equalsIgnoreCase(roleName.getName())) {
                role = true;
            }
        }
        if (role == true) {
            response.setMessage("Roles already exist");
            //Message Duplicate roleName
            response.setStatus(HttpStatus.BAD_REQUEST);
        } else {
            response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            response.setData(rolesResponse);
            response.setStatus(HttpStatus.OK);

        }
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/deleteRoleById")
    public ResponseEntity<APIResponse<String>> deleteRoleById(@Valid @RequestParam String roleName) throws NotFoundException {
        APIResponse<String> response = new APIResponse<>();
        String rolesResponse = rolesService.deleteRoleById(roleName);
        if(rolesResponse == null) {
            throw new NotFoundException("Can not find Name "+roleName);
        }
        System.out.println(rolesService.deleteRoleById(rolesResponse));
            response.setStatus(HttpStatus.OK);
            response.setData(rolesResponse);
            response.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());
        return ResponseEntity.ok().body(response);
    }
}

