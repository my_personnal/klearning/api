package com.kshrd.klearing.controller;
import ch.qos.logback.classic.spi.IThrowableProxy;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.request.SubTypeRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.response.SubTypeResponse;
import com.kshrd.klearing.repository.SubtypeRepository;
import com.kshrd.klearing.service.SubTypeService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("${baseUrl}/subtype")
public class SubTypeController {

    @Autowired
    SubTypeService subTypeService;

    @Autowired
    SubtypeRepository subtypeRepository;

    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "Subtype";
    }

    @GetMapping("/getAllSubType")
    public ResponseEntity<APIResponse<List<SubTypeResponse>>> getAllSubType() {
        BaseMessage.obj = "SubType";
        APIResponse<List<SubTypeResponse>> response = new APIResponse<>();
        List<SubTypeResponse> subTypeResponsesList = subTypeService.findAllSubTypes();
        response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(subTypeResponsesList);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/searchSubTypeByName")
    public ResponseEntity<APIResponse<List<SubTypeResponse>>> searchSubTypeByName(@Valid @RequestParam String searchByName) throws NotFoundException {
        BaseMessage.obj = "SubType";
        APIResponse<List<SubTypeResponse>> response = new APIResponse<>();
        List<SubTypeResponse> subTypeResponse = subTypeService.findSubTypeByName(searchByName);
        if(response.getData() ==null){
            throw new NotFoundException("Can not find SubTypeName: "+searchByName);
        }
        response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(subTypeResponse);

        return ResponseEntity.ok(response);
    }

    @PostMapping("/insertSubType")
    public ResponseEntity<APIResponse<SubTypeResponse>> insertSubType(@Valid @RequestBody SubTypeRequest insertSubType) {
        BaseMessage.obj = "SubType";
        APIResponse<SubTypeResponse> response = new APIResponse<>();
        List<String> subtypeName = subtypeRepository.findAllNameSubTypes();
        boolean subtypes = false;
        insertSubType.setSubType(insertSubType.getSubType().trim());
        for(int i =0;i<subtypeName.size();i++){
            if(subtypeName.get(i).equalsIgnoreCase(insertSubType.getSubType())){
                subtypes = true;
            }
        }
        if(subtypes == true){
            response.setMessage("Subtype already exist");
            //Message Duplicate subtypeName
            response.setStatus(HttpStatus.BAD_REQUEST);
        }
        else {
            SubTypeResponse subTypeResponse = subTypeService.insertSubType(insertSubType);
            response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
            response.setData(subTypeResponse);
            response.setStatus(HttpStatus.OK);
        }

        return ResponseEntity.ok(response);
    }
    @PutMapping("/updateSubTypeById")
    public ResponseEntity<APIResponse<SubTypeResponse>> updateSubTypeById(@Valid @RequestBody SubTypeResponse updateSubTypeID) throws NotFoundException  {
        BaseMessage.obj = "SubType";
        APIResponse<SubTypeResponse> response = new APIResponse<>();
        List<String> subtypeName = subtypeRepository.findAllNameSubTypes();
        boolean subtypes = false;
        SubTypeResponse subTypeResponse = subTypeService.updateSubTypeByID(updateSubTypeID);
        if(subTypeResponse ==null){
            throw new NotFoundException("Can not find subtypeID: "+updateSubTypeID);
        }
        for(int i =0;i<subtypeName.size();i++){
            if(subtypeName.get(i).equalsIgnoreCase(updateSubTypeID.getSubType())){
                subtypes = true;
            }
        }
        if(subtypes == true){
            response.setMessage("Subtype already exist");
            //Message Duplicate subtypeName
            response.setData(response.getData());
            response.setStatus(HttpStatus.BAD_REQUEST);
        }
        else {

            response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            response.setData(subTypeResponse);
            response.setStatus(HttpStatus.OK);
        }
        return ResponseEntity.ok().body(response);
    }
    @DeleteMapping("/deleteSubTypeById")
    public ResponseEntity<APIResponse<Integer>> deleteSubTypeById(@RequestParam Integer id) throws NotFoundException {
        BaseMessage.obj = "SubType";
        APIResponse<Integer> response = new APIResponse<>();
        Integer subTypeResponse = subTypeService.deleteSubTypeByID(id);
        System.out.println(subTypeService.deleteSubTypeByID(id));
        if(subTypeResponse == null){
            throw new NotFoundException("Can not find SubtypeID: "+id);
        }
        response.setStatus(HttpStatus.OK);
        response.setData(subTypeResponse);
        response.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());

        return ResponseEntity.ok().body(response);
    }
}
