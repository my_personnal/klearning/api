package com.kshrd.klearing.controller;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.request.reward.RewardRequest;
import com.kshrd.klearing.model.request.reward.RewardResponse;
import com.kshrd.klearing.model.request.reward.RewardUpdateRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.result.ResultRequest;
import com.kshrd.klearing.model.result.ResultResponse;
import com.kshrd.klearing.repository.RewardRepository;
import com.kshrd.klearing.service.RewardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/${baseUrl}/reward/")
public class RewardController {

    @Autowired
    RewardService rewardService;

    @Autowired
    RewardRepository rewardRepository;

    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "Reward";
    }

    @GetMapping("/getAllReward")
    public ResponseEntity<APIResponse<List<RewardResponse>>> getAllReward() {
        APIResponse<List<RewardResponse>> response = new APIResponse<>();
        BaseMessage.obj = "Reward";
            List<RewardResponse> rewardResponse = rewardService.getAllReward();
            if(rewardResponse == null){
                response.setMessage("All Records not found...");
                response.setStatus(HttpStatus.NOT_FOUND);
            }else {
                response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                response.setStatus(HttpStatus.OK);
                response.setData(rewardResponse);
            }
        return ResponseEntity.ok(response);
    }


    @PostMapping("/insertReward")
    public ResponseEntity<APIResponse<RewardResponse>> insertReward(@RequestBody RewardRequest rewardRequest) {
        BaseMessage.obj = "Reward";
        APIResponse<RewardResponse> response = new APIResponse<>();
        rewardRequest.setUserID(AuthenticationRestController.userId);
        List<String> nameOfResult = rewardRepository.getAllNameResult();
        boolean result = false;
        for(int i =0; i< nameOfResult.size();i++){
            if(nameOfResult.get(i).equalsIgnoreCase(rewardRequest.getName())){
                result = true;
            }
        }
        if(result){
            response.setMessage("Duplicated name...");
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
        }else {
            System.out.println("Data "+rewardRequest);
            RewardResponse rewardResponse = rewardService.insertReward(rewardRequest);
            response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(rewardResponse);
        }
        return ResponseEntity.ok(response);
    }

    @PutMapping("/updateRewardByID")
    public ResponseEntity<APIResponse<RewardResponse>> updateRewardByID(@RequestBody RewardUpdateRequest updateRewardByID) {
        APIResponse<RewardResponse> response = new APIResponse<>();
        BaseMessage.obj = "Reward";
        updateRewardByID.setUserID(AuthenticationRestController.userId);
        List<String> nameOfResult = rewardRepository.getAllNameResult();
        boolean result = false;
        System.out.println("name:"+updateRewardByID.getName());
        for(int i =0; i< nameOfResult.size();i++){
            System.out.println(nameOfResult);
            if(nameOfResult.get(i).equalsIgnoreCase(updateRewardByID.getName())){
                System.out.println("true");
                result = true;
            }
        }
        if(result){
            System.out.println("res"+result);
            response.setMessage("Duplicated name...");
            response.setStatus(HttpStatus.NOT_ACCEPTABLE);
        }else {
            System.out.println("fasle in else");
            RewardResponse rewardResponse = rewardService.updateRewardByID(updateRewardByID);
            System.out.println(rewardResponse);
            if(rewardResponse == null){
                response.setMessage("ID not found....");
                response.setStatus(HttpStatus.NOT_FOUND);
                response.setData(rewardResponse);
            }else {
                response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
                response.setStatus(HttpStatus.OK);
                response.setData(rewardResponse);
            }
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping ("/getRewardByName")
    public ResponseEntity<APIResponse<List<RewardResponse>>> getRewardByName(@RequestParam String name) {
        BaseMessage.obj = "Reward";
        APIResponse<List<RewardResponse>> response = new APIResponse<>();
        List<RewardResponse> rewardResponse = rewardService.getRewardByName(name);
            if(rewardResponse.size() == 0){
                response.setMessage(BaseMessage.Error.SELECT_ERROR.getMessage());
                response.setStatus(HttpStatus.NOT_FOUND);
            }else {
                response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                response.setStatus(HttpStatus.OK);
                response.setData(rewardResponse);
            }

        return ResponseEntity.ok(response);
    }

    @DeleteMapping ("/deleteRewardByID")
    public ResponseEntity<APIResponse<RewardResponse>> deleteRewardByID(@RequestParam Integer id) {
        BaseMessage.obj = "Reward";
        APIResponse<RewardResponse> response = new APIResponse<>();
        RewardResponse rewardResponse = rewardService.deleteRewardByID(id);
        if(rewardResponse == null){
            response.setMessage("ID was not found...");
            response.setStatus(HttpStatus.NOT_FOUND);
        }else {
            response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            response.setStatus(HttpStatus.OK);
            response.setData(rewardResponse);
        }

        return ResponseEntity.ok(response);
    }


}
