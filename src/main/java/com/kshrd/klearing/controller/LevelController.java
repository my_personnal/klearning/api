package com.kshrd.klearing.controller;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.request.EVisibility;
import com.kshrd.klearing.model.request.LevelsRequest;
import com.kshrd.klearing.model.request.UpdateLevelRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.response.LevelsResponse;
import com.kshrd.klearing.repository.LevelsRepository;
import com.kshrd.klearing.service.LevelsService;
import org.apache.ibatis.javassist.NotFoundException;
import org.mockito.internal.matchers.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

@RestController
@RequestMapping("${baseUrl}/level/")
public class LevelController {

    @Autowired
    private LevelsService levelsService;

    @Autowired
    private LevelsRepository levelsRepository;

    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "Level";
    }

    @GetMapping("/getAllLevels")
    public ResponseEntity<APIResponse<List<LevelsResponse>>> getAllLevels() {
        BaseMessage.obj = "Levels";
        APIResponse<List<LevelsResponse>> response = new APIResponse<>();
        List<LevelsResponse> levelResponsesList = levelsService.findAllLevels();
        response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(levelResponsesList);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/searchLevelByName")
    public ResponseEntity<APIResponse<List<LevelsResponse>>> searchLevelByName(@RequestParam String name) throws NotFoundException {
        BaseMessage.obj = "Levels";
        APIResponse<List<LevelsResponse>> response = new APIResponse<>();
        List<LevelsResponse> levelResponse = levelsService.findLevelByName(name);
        if(levelResponse.size() == 0){
                throw new NotFoundException("Can not find Name: "+name);
        }
        response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(levelResponse);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/insertLevel")
    public ResponseEntity<APIResponse<LevelsResponse>> insertLevel(@Valid @RequestBody LevelsRequest levelsRequest,
                                                           @RequestParam(value = "visibility", defaultValue = "Public") EVisibility visibility) {
        BaseMessage.obj = "Levels";
        if(visibility.name() == "Private"){
            levelsRequest.setVisibility(0);
        }
        if(visibility.name() == "Public"){
            levelsRequest.setVisibility(1);
        }
        if(visibility.name() == "Group"){
            levelsRequest.setVisibility(2);
        }
        APIResponse<LevelsResponse> response = new APIResponse<>();
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        java.time.LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        levelsRequest.setCreatedDate(offsetTime);
        levelsRequest.setUserID(AuthenticationRestController.userId);
        List<String> levelType = levelsRepository.findNameAllLevels();
        boolean types = false;
        for (int i = 0; i < levelType.size(); i++) {
            if (levelType.get(i).equalsIgnoreCase(levelsRequest.getName())) {
                types = true;
            }
        }
        if (types == true) {
            response.setMessage("Level already exist");
            //Message Duplicate typeName
            response.setStatus(HttpStatus.BAD_REQUEST);
        } else {
            LevelsResponse levelResponse = levelsService.insertLevel(levelsRequest);
            response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
            response.setData(levelResponse);
            response.setStatus(HttpStatus.OK);
        }
        return ResponseEntity.ok(response);
    }
    @PutMapping("/updateLevelById")
    public ResponseEntity<APIResponse<LevelsResponse>> updateLevelById(@RequestBody UpdateLevelRequest levelName,
                                                                @RequestParam(value = "visibility", defaultValue = "Public") EVisibility visibility) throws NotFoundException{

        System.out.println("hh "+levelName);
        BaseMessage.obj = "Levels";
        APIResponse<LevelsResponse> response = new APIResponse<>();

        if(visibility.name() == "Private"){
            levelName.setVisibility(0);
        }
        if(visibility.name() == "Public"){
            levelName.setVisibility(1);
        }
        if(visibility.name() == "Group"){
            levelName.setVisibility(2);
        }
        ZoneOffset zoneOffset = ZoneOffset.of("+07:00");
        ZoneId zoneId=ZoneId.ofOffset("UTC", zoneOffset);
        java.time.LocalDateTime offsetTime = LocalDateTime.now(zoneId);
        levelName.setCreatedDate(offsetTime);
        levelName.setUserID(AuthenticationRestController.userId);
        List<String> levelType = levelsRepository.findNameAllLevels();
        LevelsResponse levelResponse = null;

        boolean name = false;

        for (int i = 0; i < levelType.size(); i++) {
            System.out.println(levelType.get(i));
            if (levelName.getName().equalsIgnoreCase(levelType.get(i))) {
                name = true;
            }
        }
        if (name) {
            System.out.println("name:"+name);
            response.setMessage("Level already exist");
            //Message Duplicate levelType
            response.setStatus(HttpStatus.BAD_REQUEST);
        } else {
            levelResponse = levelsService.updateLevelByName(levelName);
            if(levelResponse == null){
                System.out.println("name in false:"+name);
                response.setMessage(BaseMessage.Error.UPDATE_ERROR.getMessage());
                response.setData(levelResponse);
                response.setStatus(HttpStatus.NOT_FOUND);
            }else {
                System.out.println("name in false:"+name);
                response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
                response.setData(levelResponse);
                response.setStatus(HttpStatus.OK);
            }

        }
        return ResponseEntity.ok().body(response);
    }
    @DeleteMapping("/deleteLevelByID")
    public ResponseEntity<APIResponse<Integer>> deleteLevelByID(@RequestParam Integer id) throws NotFoundException {
        BaseMessage.obj = "Levels";
        APIResponse<Integer> response = new APIResponse<>();
        Integer levelResponse = levelsService.deleteLevelById(id);
        if(levelResponse == null){
            throw new NotFoundException("Can not find LevelID: "+id);
        }
        response.setStatus(HttpStatus.OK);
        response.setData(levelResponse);
        response.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());
        return ResponseEntity.ok().body(response);
    }
}
