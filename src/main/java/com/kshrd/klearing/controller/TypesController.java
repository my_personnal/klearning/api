package com.kshrd.klearing.controller;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.request.TypeRequest;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.response.TypeResponse;
import com.kshrd.klearing.repository.TypeRepository;
import com.kshrd.klearing.service.TypeService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("${baseUrl}/type/")
public class TypesController {
    @Autowired
    private TypeService typeService;

    @Autowired
    private TypeRepository typeRepository;

    BaseMessage baseMessage;

    @Autowired
    public void setBaseMessage(BaseMessage baseMessage) {
        this.baseMessage = baseMessage;
        BaseMessage.obj = "Type";
    }

    @GetMapping("/getAllType")
    public ResponseEntity<APIResponse<List<TypeResponse>>> getAllType() {
        BaseMessage.obj = "Types";
        APIResponse<List<TypeResponse>> response = new APIResponse<>();
        List<TypeResponse> typeResponsesList = typeService.findAllType();
        response.setMessage(BaseMessage.Success.SELECT_ALL_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(typeResponsesList);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/searchTypeByName")
    public ResponseEntity<APIResponse<List<TypeResponse>>> searchTypeByName(@RequestParam String searchTypeByName) throws NotFoundException {
        BaseMessage.obj = "Types";
        APIResponse<List<TypeResponse>> response = new APIResponse<>();
        List<TypeResponse> typeResponse = typeService.findTypeByName(searchTypeByName);
        if(typeResponse.size() == 0){
            throw new NotFoundException("Can not find RoleName:  "+searchTypeByName);
        }
        response.setMessage(BaseMessage.Success.SELECT_ONE_RECORD_SUCCESS.getMessage());
        response.setStatus(HttpStatus.OK);
        response.setData(typeResponse);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/insertType")
    public ResponseEntity<APIResponse<TypeResponse>> insertType(@Valid @RequestBody TypeRequest insertType) {
        BaseMessage.obj = "Types";
        APIResponse<TypeResponse> response = new APIResponse<>();
        List<String> typeName = typeRepository.findNameAllTypes();
        boolean types = false;
        for (int i = 0; i < typeName.size(); i++) {
            if (typeName.get(i).equalsIgnoreCase(insertType.getName())) {
                types = true;
            }
        }
        if (types == true) {
            response.setMessage("Type already exist");
            //Message Duplicate typeName
            response.setStatus(HttpStatus.BAD_REQUEST);
        } else {
            TypeResponse typeResponse = typeService.insertType(insertType);
            response.setMessage(BaseMessage.Success.INSERT_SUCCESS.getMessage());
            response.setData(typeResponse);
            response.setStatus(HttpStatus.OK);
        }
        return ResponseEntity.ok(response);
    }
    @PutMapping("/updateTypeById")
    public ResponseEntity<APIResponse<TypeResponse>> updateTypeById(@Valid @RequestBody TypeResponse updateTypeById) throws NotFoundException {
        BaseMessage.obj = "Types";
        APIResponse<TypeResponse> response = new APIResponse<>();
        List<String> typeName = typeRepository.findNameAllTypes();
        TypeResponse typeResponse = typeService.updateTypeByName(updateTypeById);
        if (typeResponse == null) {
            throw new NotFoundException("Can not find with type: " + updateTypeById);
        }
        boolean types = false;
        for (int i = 0; i < typeName.size(); i++) {
            if (typeName.get(i).equalsIgnoreCase(updateTypeById.getName())) {
                types = true;
            }
        }
        if (types == true) {
            response.setMessage("Type already exist");
            //Message Duplicate typeName
            response.setStatus(HttpStatus.BAD_REQUEST);
        } else {
            response.setMessage(BaseMessage.Success.UPDATE_SUCCESS.getMessage());
            response.setData(typeResponse);
            response.setStatus(HttpStatus.OK);
        }

        return ResponseEntity.ok().body(response);
    }
    @DeleteMapping("/deleteTypeByName")
    public ResponseEntity<APIResponse<String>> deleteTypeByName(@RequestParam String typeName) throws NotFoundException{
        APIResponse<String> response = new APIResponse<>();
        String typeResponse = typeService.deleteTypeByName(typeName);
        if(typeResponse == null){
            throw new NotFoundException("Can not find role name: "+typeName);
        }
        System.out.println(typeService.deleteTypeByName(typeName));
        response.setStatus(HttpStatus.OK);
        response.setData(typeResponse);
        response.setMessage(BaseMessage.Success.DELETE_SUCCESS.getMessage());

        return ResponseEntity.ok().body(response);

    }
}
