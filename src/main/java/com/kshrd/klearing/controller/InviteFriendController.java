package com.kshrd.klearing.controller;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/invite-friend/")
public class InviteFriendController {

    @PostMapping("{id}")
    public ResponseEntity<?> getInviteById(@PathVariable Integer id){
        return ResponseEntity.ok().body("Invite");
    }
    @PostMapping()
    public ResponseEntity<?> InsertInvite(@PathVariable Integer id){
        return ResponseEntity.ok().body("Invite");
    }
    @PutMapping("{id}")
    public ResponseEntity<?> updateInviteById(@PathVariable Integer id){
        return ResponseEntity.ok().body("Invite");
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteLevelById(@PathVariable Integer id){
        return ResponseEntity.ok().body("Invite");
    }
}
