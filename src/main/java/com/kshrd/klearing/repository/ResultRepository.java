package com.kshrd.klearing.repository;
import com.kshrd.klearing.model.request.UpdateUserRequest;
import com.kshrd.klearing.model.result.ResultRequest;
import com.kshrd.klearing.model.result.ResultResponse;
import com.kshrd.klearing.model.result.ResultUpdateRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository {

    @Select("select * from results")
    @Results(id="mapping", value =
            {
                    @Result(property = "userAnswers", column = "user_answer",typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
                    @Result(property = "totalScore",column = "total_score"),
                    @Result(property = "userScore", column = "user_score"),
                    @Result(property = "materialID", column = "material_id"),
                    @Result(property = "userID", column = "user_id")
            }
    )
    List<ResultResponse> getAllResults ();

    @Select("Select * FROM results WHERE id = #{id}")
    @ResultMap("mapping")
    List<ResultResponse>findAllResultByName(Integer id);


    @Select("delete from results where id =#{id} returning id,user_answer,total_score,user_score,material_id,user_id")
    @ResultMap("mapping")
    ResultResponse deleteResultByID(Integer id);


    @Select("insert into results(user_answer,total_score,user_score,material_id,user_id) VALUES (#{result.userAnswers,jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler},#{result.totalScore}," +
            "#{result.userScore},#{result.materialID},#{result.userID}) returning id,user_answer,total_score,user_score,material_id,user_id")
    @ResultMap("mapping")
    ResultResponse insertResult(@Param("result") ResultRequest resultRequest);


    @Select("UPDATE results SET user_answer = #{result.userAnswers,jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler},total_score=#{result.totalScore},user_score = #{result.userScore},material_id=#{result.materialID},user_id=#{result.userID}WHERE id = #{result.id} returning id,user_answer,total_score,user_score,material_id,user_id")
    @ResultMap("mapping")
    ResultResponse updateResultById(@Param("result") ResultUpdateRequest resultResponse);


    @Select("select * FROM materials")
    List<Integer> getAllMaterialID();
}

