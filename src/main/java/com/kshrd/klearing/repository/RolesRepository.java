package com.kshrd.klearing.repository;
import com.kshrd.klearing.model.request.RolesRequest;
import com.kshrd.klearing.model.response.Roles;
import com.kshrd.klearing.model.response.RolesResponse;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolesRepository {

        @Select("SELECT * FROM roles order by id")
         List<RolesResponse> findAllRoles();

        @Select("SELECT name FROM roles order by id")
        List<RolesResponse> findAllRolesForUser();
        @Select("SELECT * FROM roles WHERE name ILIKE #{roleName} || '%' ")
        List<RolesResponse> findRoleByName(String roleName);

        @Select("INSERT INTO roles (name) VALUES (#{role.name}) RETURNING id,name")
        RolesResponse insertRoles(@Param("role") RolesRequest rolesRequest);

        @Select("DELETE FROM roles WHERE name = #{roleName} RETURNING name")
        String deleteRoleById(String roleName);

        @Select("UPDATE roles " +
            "SET name = #{role.name} " +
            "WHERE id = #{role.id} RETURNING id,name")
        RolesResponse updateRoleByName(@Param("role") RolesResponse roleName);


        @Select("SELECT name FROM roles order by id")
        List<String> findNameAllRoles();
}
