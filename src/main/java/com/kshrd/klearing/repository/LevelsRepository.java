package com.kshrd.klearing.repository;


import com.kshrd.klearing.model.request.LevelsRequest;
import com.kshrd.klearing.model.request.UpdateLevelRequest;
import com.kshrd.klearing.model.response.LevelsResponse;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LevelsRepository {

    @Select("SELECT * FROM levels order by id")
    @Results(id = "mapLevel", value =
            {
                    @Result(property = "createdDate", column = "created_date"),
                    @Result(property = "userID",column = "user_id")
            }
        )
    List<LevelsResponse> findAllLevels();
    @Select("SELECT * FROM levels WHERE name ILIKE #{levelName} || '%' ")
    @ResultMap("mapLevel")
    List<LevelsResponse> findLevelByName(String levelName);

    @Select("INSERT INTO levels (name,visibility,created_date,user_id) VALUES (#{level.name},#{level.visibility},#{level.createdDate},#{level.userID}) RETURNING id,name,visibility,created_date,user_id")
    @ResultMap("mapLevel")
    LevelsResponse insertLevel(@Param("level") LevelsRequest levelsRequest);

    @Select("DELETE FROM levels WHERE id = #{id} RETURNING id")
    Integer deleteLevelById(Integer id);

    @Select("UPDATE levels " +
            "SET name = #{level.name}," +
            "visibility = #{level.visibility}," +
            "created_date = #{level.createdDate}," +
            "user_id = #{level.userID} " +
            "WHERE id = #{level.id} RETURNING id,name,visibility,created_date,user_id")
    @ResultMap("mapLevel")
    LevelsResponse updateLevelByName(@Param("level") UpdateLevelRequest levelsResponse);


    @Select("SELECT name FROM levels")
    List<String> findNameAllLevels();
}
