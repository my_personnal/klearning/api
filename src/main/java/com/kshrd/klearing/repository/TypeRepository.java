package com.kshrd.klearing.repository;


import com.kshrd.klearing.model.request.RolesRequest;
import com.kshrd.klearing.model.request.TypeRequest;
import com.kshrd.klearing.model.response.RolesResponse;
import com.kshrd.klearing.model.response.TypeResponse;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeRepository {

    @Select("SELECT * FROM types order by id")
    List<TypeResponse> findAllTypes();

    @Select("SELECT name FROM types order by id")
    List<String> findNameAllTypes();
    @Select("SELECT * FROM types WHERE name ILIKE #{typeName} || '%' ")
    List<TypeResponse> findTypeByName(String typeName);

    @Select("INSERT INTO types (name) VALUES (#{type.name}) RETURNING id,name")
    TypeResponse insertType(@Param("type") TypeRequest typeRequest);

    @Select("DELETE FROM types WHERE name = #{typeName} RETURNING name")
    String deleteTypeByName(String typeName);

    @Select("UPDATE types " +
            "SET name = #{type.name} " +
            "WHERE id = #{type.id} RETURNING id,name")
    TypeResponse updateTypeByName(@Param("type") TypeResponse typeResponse);
}
