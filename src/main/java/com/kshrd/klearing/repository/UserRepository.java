package com.kshrd.klearing.repository;
import com.kshrd.klearing.model.request.UpdateUserRequest;
import com.kshrd.klearing.model.request.UserRequest;
import com.kshrd.klearing.model.response.UserResponse;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {
    @Select("select * from users where status is true")
    @Results( id = "mapUser", value =
            {
                    @Result(property = "registerDate", column = "register_date"),
                    @Result(property = "roleId",column = "roles_id")
            }
    )
    List<UserResponse> getAllUsers ();


    @Select("SELECT * FROM users WHERE username ILIKE '%'||#{name}||'%' AND status is true ")
    @ResultMap("mapUser")
    List<UserResponse> findUserByName(String name);

    @Select("Update users set status = false where id =#{id} returning username")
    String deleteUserByStatus(Integer id);


    @Select("insert into users(name,username,email,gender,password,roles_id,register_date,status,image) VALUES (#{user.name},#{user.username}," +
            "#{user.email},#{user.gender},#{user.password},#{user.role_id},#{user.registerDate},#{user.status},#{user.image}) returning id,name,username,email,gender,roles_id,register_date,status,image")
    @ResultMap("mapUser")
    UserResponse insertUser(@Param("user") UserRequest userRequest);


    @Select("UPDATE users SET name = #{user.name},username=#{user.username},gender=#{user.gender},email=#{user.email},register_date=#{user.registerDate},roles_id=#{user.roleID},status=#{user.status},image=#{user.image} WHERE id = #{user.id} and status is true RETURNING id,name,username,gender,email,password,password,register_date,roles_id,status,image")
    @ResultMap("mapUser")
    UserResponse updateUserById(@Param("user") UpdateUserRequest updateUserRequest);


    @Select("SELECT username FROM users")
    List<String> getAllUsername();

    @Select("SELECT email FROM users")
    List<String> getAllEmail();
}
