package com.kshrd.klearing.repository;


import com.kshrd.klearing.model.request.SubTypeRequest;
import com.kshrd.klearing.model.request.TypeRequest;
import com.kshrd.klearing.model.response.SubTypeResponse;
import com.kshrd.klearing.model.response.TypeResponse;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubtypeRepository {

    @Select("SELECT sub_type FROM subtype")
    @Results(id = "mapSubType", value ={
            @Result(property = "subType", column = "sub_type")

    }
    )
    List<String> findAllNameSubTypes();
    @Select("SELECT * FROM subtype WHERE sub_type ILIKE #{subTypeName} || '%' ")
    @ResultMap("map")
    List<SubTypeResponse> findSubTypeByName(String subTypeName);

    @Select("SELECT id,sub_type FROM subtype")
    @Results(id="map", value ={
            @Result(property = "id",column = "id"),
            @Result(property = "subType", column = "sub_type")
    }
    )
    List<SubTypeResponse> findAllSubTypes();

    @Select("INSERT INTO subtype (sub_type) VALUES (#{subtype.subType}) RETURNING id,sub_type")
    @ResultMap("map")
    SubTypeResponse insertSubType(@Param("subtype") SubTypeRequest subTypeRequest);

    @Select("DELETE FROM subtype WHERE id = #{subTypeId} RETURNING id")
    Integer deleteSubTypeByName(Integer subTypeId);

    @Select("UPDATE subtype " +
            "SET sub_type = #{subtype.subType} " +
            "WHERE id = #{subtype.id} RETURNING id,sub_type")
    @ResultMap("map")
    SubTypeResponse updateSubTypeByID(@Param("subtype") SubTypeResponse subTypeResponse);
}
