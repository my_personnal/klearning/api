package com.kshrd.klearing.repository;


import com.kshrd.klearing.model.AuthenticationUsers;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthenticationUserRepository {

    @Select("select u.username,u.password,r.name as role from users u join roles r on u.roles_id = r.id where u.username=#{username}")
    AuthenticationUsers loadUser(@Param("username") String username);

    @Select("select username from users")
    List<String> getUsername();

    @Select("select status from users where username=#{username}")
    Boolean getStatus(@Param("username") String username);

    @Select("select id from users where username=#{username}")
    int selectUserID(@Param("username") String username);

}

