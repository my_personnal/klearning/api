package com.kshrd.klearing.repository;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PracticeRepository {

    @Select("SELECT id from types")
    List<Integer> getAllTypeID();

    @Select("SELECT id from subtype")
    List<Integer> getAllSubTypeID();

    @Select("SELECT id from levels")
    List<Integer> getAllLevel();

    @Select("SELECT name from materials")
    List<String> getAllNameMaterial();
}
