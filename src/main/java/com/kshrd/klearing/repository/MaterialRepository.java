package com.kshrd.klearing.repository;
import com.kshrd.klearing.model.materials.*;
import com.kshrd.klearing.model.materials.content.Content;
import com.kshrd.klearing.model.materials.MTPracticeFillingTheBlankRequest;
import com.kshrd.klearing.model.materials.content.SubType;
import com.kshrd.klearing.model.materials.content.practice.PracticeContent;
import com.kshrd.klearing.model.materials.content.practice.PracticeFillingTheBlank;
import com.kshrd.klearing.model.request.UpdateAllMaterialRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MaterialRepository {


    @Select("INSERT INTO materials(name,description,create_date,content,type_id,user_id,level_id,status,subtype_id) values(" +
            "#{material.name}," +
            "#{material.description}," +
            "#{material.createdDate}," +
            "#{material.content,jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "#{material.typeID}," +
            "#{material.userId}," +
            "#{material.levelID}," +
            "#{material.status}," +
            "#{material.subTypeID}) RETURNING id,name,description,create_date,content,type_id,user_id,level_id,status,count,subtype_id"
    )
    @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    }
    )
    MaterialResponse insertMaterial(@Param("material") MaterialRequest materialRequest);

    @Select("INSERT INTO materials(name,description,create_date,content,type_id,user_id,level_id,status,subtype_id) values(" +
            "#{mtVocabForCon.name}," +
            "#{mtVocabForCon.description}," +
            "#{mtVocabForCon.createdDate}," +
            "#{mtVocabForCon.content,jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "#{mtVocabForCon.typeID}," +
            "#{mtVocabForCon.userID}," +
            "#{mtVocabForCon.levelID}," +
            "#{mtVocabForCon.status}," +
            "#{mtVocabForCon.subTypeID}) RETURNING id,name,description,create_date,content,type_id,user_id,level_id,status,count,subtype_id"
    )
    @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    }
    )
    MTVocabForConsonantResponse insertMTVocabForConsonant(@Param("mtVocabForCon") MTVocabForConsonantRequest mtVocabForConsonantRequest);
    @Select("select materials.id,sub_type from materials inner " +
            "join subtype s on materials.subtype_id  = s.id " +
            "join types t on t.id = materials.type_id " +
            "join levels l on l.id = materials.level_id " +
            "where level_id=#{levelId} and type_id = #{typeId}")
    @Results(
            @Result(property = "subType", column = "sub_type")
    )

    List<SubType> getSubTypeByLevelIdAndTypeId(Integer levelId, Integer typeId);


    @Select("select content from materials inner " +
            "    join subtype s on materials.subtype_id  = s.id " +
            "join types t on t.id = materials.type_id " +
            "join levels l on l.id = materials.level_id " +
            "where level_id=#{levelId} and type_id =#{typeId} and subtype_id =#{subtypeId}")
    @Results(
            @Result(property ="allMaterialElements",column = "content",typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class)
    )
    List<Content> getContentByTypeIDLevelIdAndSubTypeId(Integer levelId, Integer typeId, Integer subtypeId);


    @Select("select * from materials where level_id = #{levelID} and subtype_id =#{subtypeID} and type_id= #{typeID}")
    @Results(id = "mapMaterial", value = {
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    }
    )
    List<MaterialResponse> getAllMaterial(@Param("levelID") Integer levelID,@Param("subtypeID")Integer subtypeID,@Param("typeID") Integer typeID);

    @Select("INSERT INTO materials(name,description,create_date,type_id,user_id,level_id,status,subtype_id) values(" +
            "#{material.name}," +
            "#{material.description}," +
            "#{material.createdDate}," +
            "#{material.typeID}," +
            "#{material.userID}," +
            "#{material.levelID}," +
            "#{material.status}," +
            "#{material.subTypeID}) RETURNING id,name,description,create_date,type_id,user_id,level_id,status,count,subtype_id"
    )
    @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    })
    HeaderOfMaterialTypeResponse InsertHeaderOfMaterialType(@Param("material") HeaderOfMaterialTypeRequest materialRequest);


    @Select("UPDATE materials SET " +
            "id = #{material.id}," +
            "name = #{material.name}," +
            "description = #{material.description}," +
            "create_date = #{material.createdDate}," +
            "content = #{material.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "type_id = #{material.typeID}," +
            "user_id = #{material.userId}," +
            "level_id = #{material.levelID}," +
            "status = #{material.status}," +
            "subtype_id = #{material.subTypeID}" +
            "WHERE id = #{material.id} RETURNING id,name,description,create_date,content,type_id,user_id,level_id,status,count,subtype_id")
    @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    })//hello
    UpdateAllMaterialRequest updateMaterialById(@Param("material") UpdateAllMaterialRequest updateAllMaterialRequest);

    @Select("UPDATE materials SET " +
            "id = #{material.id}," +
            "content = #{material.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler} " +
            "WHERE id = #{material.id} RETURNING id,name,description,create_date,content,type_id,user_id,level_id,status,count,subtype_id")
    @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    })
    MTVocabForConsonantResponse updateAllVocabForConsonantById(@Param("material") UpdateAllMTVocabForConsonantRequest updateMTVocabForConsonantRequest);

    @Select("select * from materials where level_id = #{levelID} and subtype_id =#{subtypeID} and type_id= #{typeID}")
    @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    })
    List<MTVocabForConsonantResponse> getAllVocabForConsonant(@Param("levelID") Integer levelID,@Param("subtypeID")Integer subtypeID,@Param("typeID") Integer typeID);

    @Select("select id,name,description,create_date from materials where type_id = #{levelID} and level_id = #{subtypeID}")
    @Results(
    @Result(property = "createdDate", column = "create_date")
    )
    List<MaterialInfoTypeResponse> getMaterialInfo(@Param("levelID") Integer levelID,@Param("subtypeID")Integer subtypeID);


    @Select("select content from materials inner " +
            "join subtype s on materials.subtype_id  = s.id " +
            "join types t on t.id = materials.type_id " +
            "join levels l on l.id = materials.level_id " +
            "where level_id=#{levelId} and type_id =#{typeId} and subtype_id =#{subtypeId}")
    @Results(
            @Result(property ="practiceItems", column = "content",typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class)
    )
    List<PracticeContent> getPracticeContentByTypeIDLevelIdAndSubTypeId(@Param("levelId") Integer levelId, @Param("subtypeId") Integer subtypeID, @Param("typeId") Integer typeID);


    @Select("INSERT INTO materials(name,description,create_date,content,type_id,user_id,level_id,status,subtype_id) values(" +
            "#{mtPracticeMultipleChoice.name}," +
            "#{mtPracticeMultipleChoice.description}," +
            "#{mtPracticeMultipleChoice.createdDate}," +
            "#{mtPracticeMultipleChoice.content,jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "#{mtPracticeMultipleChoice.typeID}," +
            "#{mtPracticeMultipleChoice.userId}," +
            "#{mtPracticeMultipleChoice.levelID}," +
            "#{mtPracticeMultipleChoice.status}," +
            "#{mtPracticeMultipleChoice.subTypeID}) RETURNING id,name,description,create_date,content,type_id,user_id,level_id,status,count,subtype_id"
    )
    @Results(id="allMaterial", value = {
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    })

    MTPracticeResponse insertPracticeContent(@Param("mtPracticeMultipleChoice") MTPracticeRequest updateMTPracticeMultipleChoiceRequest);

    @Select("INSERT INTO materials(name,description,create_date,content,type_id,user_id,level_id,status,subtype_id) values(" +
            "#{mtPracticeFillingTheBlank.name}," +
            "#{mtPracticeFillingTheBlank.description}," +
            "#{mtPracticeFillingTheBlank.createdDate}," +
            "#{mtPracticeFillingTheBlank.content,jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "#{mtPracticeFillingTheBlank.typeID}," +
            "#{mtPracticeFillingTheBlank.userId}," +
            "#{mtPracticeFillingTheBlank.levelID}," +
            "#{mtPracticeFillingTheBlank.status}," +
            "#{mtPracticeFillingTheBlank.subTypeID}) RETURNING id,name,description,create_date,content,type_id,user_id,level_id,status,count,subtype_id"
    )
    @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    })
    MTPracticeFillingTheBlankResponse mtPracticeFillingTheBlankRequest(@Param("mtPracticeFillingTheBlank") MTPracticeFillingTheBlankRequest mtPracticeFillingTheBlankRequest);


    @Select("select content from materials inner " +
            "join subtype s on materials.subtype_id  = s.id " +
            "join types t on t.id = materials.type_id " +
            "join levels l on l.id = materials.level_id " +
            "where level_id=#{levelId} and type_id =#{typeId} and subtype_id =#{subtypeId}")
    @Results(
            @Result(property ="fillingTheBlankItem", column = "content",typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class)
    )
    List<PracticeFillingTheBlank> getPracticeFillingTheBlankContentByTypeIDLevelIdAndSubTypeId(Integer levelId, Integer subtypeId, Integer typeId);


    @Select("UPDATE materials SET " +
            "id = #{practice.id}," +
            "content = #{practice.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler} " +
            "WHERE id = #{practice.id} RETURNING id,content")//hello
    @Results(
            @Result(property ="content", column = "content",typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class)
    )
    MTPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest(@Param("practice") MTPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest);

    @Select("UPDATE materials SET " +
            "id = #{practice.id}," +
            "content = #{practice.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler} " +
            "WHERE id = #{practice.id} RETURNING id,content")
    @Results(
            @Result(property ="content", column = "content",typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class)
    )
    MTPracticeContentUpdateRequest UpdatePracticeContentByID(@Param("practice") MTPracticeContentUpdateRequest mtPracticeContentUpdateRequest);


    @Select("DELETE FROM materials WHERE id = #{material} RETURNING id,name")
    DeleteResponse deleteMaterialById(@Param("material") Integer deleteMaterialById);


    @Select("UPDATE materials SET " +
            "id = #{material.id}," +
            "content = #{material.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler} " +
            "WHERE id = #{material.id} RETURNING id,content")
    @Results({
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class)
    })
    MaterialUpdateRequest updateMaterialContentById(@Param("material") MaterialUpdateRequest materialUpdateRequest);


    @Select("UPDATE materials SET " +
            "id = #{material.id}," +
            "content = #{material.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler} " +
            "WHERE id = #{material.id} RETURNING id,content")
    @Results({
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class)
    })
    UpdateMTVocabForConsonantResponse updateVocabForConsonantById(@Param("material") UpdateMTVocabForConsonantRequest updateVocabForConsonantById);


    @Select("UPDATE materials SET " +
            "id = #{material.id}," +
            "name = #{material.name}," +
            "description = #{material.description}," +
            "create_date = #{material.createdDate}," +
            "content = #{material.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "type_id = #{material.typeID}," +
            "user_id = #{material.userId}," +
            "level_id = #{material.levelID}," +
            "status = #{material.status}," +
            "subtype_id = #{material.subTypeID}" +
            "WHERE id = #{material.id} RETURNING id,name,description,create_date,content,type_id,user_id,level_id,status,count,subtype_id")
          @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    })//hello
    MTAllPracticeFillingTheBlankUpdateResponse mtAllPracticeContentUpdateRequest(@Param("material") MTAllPracticeContentUpdateRequest mtAllPracticeContentUpdateRequest);

    @Select("UPDATE materials SET " +
            "id = #{material.id}," +
            "content = #{material.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler} " +
            "WHERE id = #{material.id} RETURNING id")
    Integer UpdatePracticeFillingTheBlankContentByID(MTAllPracticeContentUpdateRequest mtAllPracticeContentUpdateRequest);


    @Select("UPDATE materials SET " +
            "id = #{material.id}," +
            "name = #{material.name}," +
            "description = #{material.description}," +
            "create_date = #{material.createdDate}," +
            "content = #{material.content, jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "type_id = #{material.typeID}," +
            "user_id = #{material.userId}," +
            "level_id = #{material.levelID}," +
            "status = #{material.status}," +
            "subtype_id = #{material.subTypeID}" +
            "WHERE id = #{material.id} RETURNING id,name,description,create_date,content,type_id,user_id,level_id,status,count,subtype_id")
    @Results({
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class),
            @Result(property = "typeID", column = "type_id"),
            @Result(property = "levelID", column = "level_id"),
            @Result(property = "createdDate", column = "create_date"),
            @Result(property = "subTypeID", column = "subtype_id")
    })//hello
    MTAllPracticeFillingTheBlankUpdateResponse mtAllPracticeFillingTheBlankUpdateRequest(@Param("material") MTAllPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest);
}
