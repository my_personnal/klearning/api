package com.kshrd.klearing.repository;
import com.kshrd.klearing.model.request.reward.RewardRequest;
import com.kshrd.klearing.model.request.reward.RewardResponse;
import com.kshrd.klearing.model.request.reward.RewardUpdateRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RewardRepository {


    @Select("INSERT INTO rewards(name,content,user_id) VALUES (" +
            "#{rewardRequest.name}," +
            "#{rewardRequest.content,jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "#{rewardRequest.userID}) RETURNING id,name,content,user_id"
    )
    @Results(id="mapReward", value = {
            @Result(property = "userID", column = "user_id"),
            @Result(property = "content", column = "content", typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler.class)
})
    RewardResponse insertReward(@Param("rewardRequest") RewardRequest rewardRequest);



    @Select("Select name from rewards")
    List<String> getAllNameResult();

    @Select("UPDATE rewards set " +
            "name = #{reward.name}," +
            "content = #{reward.content,jdbcType=OTHER, typeHandler = com.kshrd.klearing.configuration.Json.JsonTypeHandler}," +
            "user_id = #{reward.userID} where id = #{reward.id} RETURNING id,name,content,user_id"
    )
    @ResultMap("mapReward")
    RewardResponse updateRewardByID(@Param("reward") RewardUpdateRequest updateRewardByID);


    @Select("SELECT * FROM rewards WHERE name ILIKE #{name} || '%' ")
    @ResultMap("mapReward")
    List<RewardResponse> getRewardByName(String name);


    @Select("Delete FROM rewards WHERE id = #{id} returning id,name,content,user_id")
    @ResultMap("mapReward")
    RewardResponse deleteRewardByID(Integer id);

    @Select("SELECT * FROM rewards")
    @ResultMap("mapReward")
    List<RewardResponse> getAllReward();
}
