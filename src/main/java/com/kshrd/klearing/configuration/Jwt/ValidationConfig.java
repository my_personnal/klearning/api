package com.kshrd.klearing.configuration.Jwt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@Configuration
public class ValidationConfig {
    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        final MethodValidationPostProcessor methodValidationPostProcessor;
        methodValidationPostProcessor = new MethodValidationPostProcessor();
        methodValidationPostProcessor.setProxyTargetClass(true);
        return methodValidationPostProcessor;
    }
}