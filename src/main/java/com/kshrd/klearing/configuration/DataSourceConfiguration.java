package com.kshrd.klearing.configuration;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@Configuration
public class DataSourceConfiguration {
    @Bean
    public javax.sql.DataSource myDB(){
        DataSourceBuilder builder = DataSourceBuilder.create();
        builder.driverClassName("org.postgresql.Driver");

        builder.url("jdbc:postgresql://110.74.194.124:5430/klearning");
        builder.username("klearning");
        builder.password("klearning");
        return builder.build();
    }
    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

}
