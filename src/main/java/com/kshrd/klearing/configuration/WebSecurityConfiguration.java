package com.kshrd.klearing.configuration;

import com.kshrd.klearing.configuration.Jwt.RequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    RequestFilter filter;
    @Autowired
    AuthenticationEntryPoint authenticationEntryPoint;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserDetailsService userDetailsService;

    @Autowired
    public void setUserDetailsService(@Qualifier("authenticationUserServiceImp") UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .csrf().disable()
                .authorizeRequests().and().
        authorizeRequests()
               .antMatchers(
                       HttpMethod.GET,
                        "/",
                        "/v2/api-docs",           // swagger
                        "/webjars/**",            // swagger-ui webjars
                        "/swagger-resources/**",  // swagger-ui resources
                        "/configuration/**",      // swagger configuration
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js"
                ).permitAll()
                .antMatchers("/api/v1/authentication/**").permitAll()
                .antMatchers("/api/v1/email/**").permitAll()

                // File
                .antMatchers("/api/v1/files/fileUpload").authenticated()
                .antMatchers("/api/v1/files/downloadFile").authenticated()

                //  roles
                .antMatchers("/api/v1/role/**").hasAnyAuthority("Admin")

                // type
                .antMatchers("/api/v1/type/getAllType").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/type/searchTypeByName").authenticated()
                .antMatchers("/api/v1/type/insertType").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/type/updateTypeById").authenticated()
                .antMatchers("/api/v1/type/deleteTypeByName").hasAnyAuthority("Admin")
                // user
                .antMatchers("/api/v1/user/getAllUser").authenticated()
                .antMatchers("/api/v1/user/searchUserByName").authenticated()
                .antMatchers("/api/v1/user/updateUserById").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/user/deleteUserByStatus").hasAnyAuthority("Admin")
                //level
                .antMatchers("/api/v1/level/getAllLevels").authenticated()
                .antMatchers("/api/v1/level/searchLevelByName").authenticated()
                .antMatchers("/api/v1/level/insertLevel").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/level/updateLevelById").authenticated()
                .antMatchers("/api/v1/level/deleteLevelByID").hasAnyAuthority("Admin")
                //material
                .antMatchers("/api/v1/material/InsertMaterial").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/level/InsertVocabForConsonant").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/level/UpdateContentByMaterialId").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/level/UpdateMaterialById").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/level/getSubTypeByLevelIdAndTypeId").authenticated()
                .antMatchers("/api/v1/level/getContentByTypeIDLevelIdAndSubTypeId").authenticated()
                .antMatchers("/api/v1/level/getMaterialBySubTypeIDLevelIDAndTypeID").authenticated()
                .antMatchers("/api/v1/level/updateAllVocabForConsonantById").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/level/updateVocabForConsonantById").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/level/getMaterialInfoByTypeIDAndLevelID").authenticated()
                .antMatchers("/api/v1/level/deleteMaterialById").hasAnyAuthority("Admin")

                //result
                .antMatchers("/api/v1/result/getAllResults").authenticated()
                .antMatchers("/api/v1/result/searchResultBy").authenticated()
                .antMatchers("/api/v1/result/insertResult").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/result/updateResultById").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/result/deleteResultById").hasAnyAuthority("Admin")
                //subtype
                .antMatchers("/api/v1/subtype/getAllSubType").authenticated()
                .antMatchers("/api/v1/subtype/searchSubTypeByName").authenticated()
                .antMatchers("/api/v1/subtype/insertSubType").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/subtype/updateSubTypeById").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/subtype/deleteSubTypeById").hasAnyAuthority("Admin")
                //practice
                .antMatchers("/api/v1/practice/getPracticeContentByTypeIDLevelIdAndSubTypeId").authenticated()
                .antMatchers("/api/v1/practice/getPracticeFillingTheBlankContentByTypeIDLevelIdAndSubTypeId").authenticated()
                .antMatchers("/api/v1/practice/InsertPracticeContent").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/practice/InsertPracticeFillingTheBlankContent").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/practice/UpdatePracticeFillingTheBlankContentByID").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/practice/UpdateAllPracticeFillingTheBlankContentByID").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/practice/UpdateAllPracticeContentByID").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/practice/UpdatePracticeContentByID").hasAnyAuthority("Admin")
                //reward
                .antMatchers("/api/v1/reward/insertReward").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/reward/updateRewardByID").hasAnyAuthority("Admin")
                .antMatchers("/api/v1/reward/getRewardByName").authenticated()
                .antMatchers("/api/v1/reward/getAllReward").authenticated()
                .antMatchers("/api/v1/reward/deleteRewardByID").hasAnyAuthority("Admin")
                //other
                .antMatchers("/configuration/ui","/webjars/**","/swagger-ui.html","/swagger-resources","/configuration/security","/v2/api-docs").permitAll()
                .and()
                .authorizeRequests().anyRequest().authenticated()
                .and().exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(
                filter, UsernamePasswordAuthenticationFilter.class);
    }
    @Override
    @Bean
    public AuthenticationManager authenticationManager() throws Exception{
        return super.authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/image/**",
                "/webjars/**");
        web.ignoring().antMatchers(HttpMethod.OPTIONS,"/**");
    }
}
