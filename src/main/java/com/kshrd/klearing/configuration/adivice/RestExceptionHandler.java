package com.kshrd.klearing.configuration.adivice;
import com.kshrd.klearing.Message.BaseMessage;
import com.kshrd.klearing.model.response.APIResponse;
import com.kshrd.klearing.model.response.APIResponseNotFoundException;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class RestExceptionHandler {

    // validate null annotation
//    @ExceptionHandler(value = MethodArgumentNotValidException.class)
//    protected ResponseEntity<APIResponse<String>> handleGlobalExceptions(MethodArgumentNotValidException ex) {
//        APIResponse<String> response = new APIResponse<>();
//        System.out.println("Update in Ex");
//        response.setStatus(HttpStatus.BAD_REQUEST);
//        response.setData( ex.getBindingResult().getFieldErrors().stream().map(err -> err.ge())
//                .collect(java.util.stream.Collectors.joining(", ")));
//        response.setMessage(ex.getBindingResult().getFieldErrors().stream().map(err -> err.getDefaultMessage())
//                .collect(java.util.stream.Collectors.joining(", ")));
//        return ResponseEntity.ok(response);
//    }
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    protected ResponseEntity<APIResponseNotFoundException<String>> handleGlobalExceptions(MethodArgumentNotValidException ex) {
        APIResponseNotFoundException<String> response = new APIResponseNotFoundException<>();
        System.out.println("Update in Ex");
        response.setStatus(HttpStatus.BAD_REQUEST);
//        response.setData( ex.getBindingResult().getFieldErrors().stream().map(err -> err.ge())
//                .collect(java.util.stream.Collectors.joining(", ")));
        response.setMessage(ex.getBindingResult().getFieldErrors().stream().map(err -> err.getDefaultMessage())
                .collect(java.util.stream.Collectors.joining(", ")));
        return ResponseEntity.ok(response);
    }

//    //not found exception
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<APIResponseNotFoundException<String>> notFoundExceptionHandler(NotFoundException e){
        APIResponseNotFoundException<String> response = new APIResponseNotFoundException<>();
        response.setMessage(e.getMessage());
        response.setStatus(HttpStatus.BAD_REQUEST);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response) ;
    }
}
