package com.kshrd.klearing.model.response;
import com.kshrd.klearing.repository.RolesRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Roles {

    @Autowired
   private RolesRepository rolesRepository;

    @PostConstruct
    public List<RolesResponse> Roles() {
        System.out.println(rolesRepository.findAllRolesForUser());
        return rolesRepository.findAllRolesForUser();
    }
}
