package com.kshrd.klearing.model.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
//@AllArgsConstructor
@NoArgsConstructor
public class RolesResponse {
   private int id;
   private String name;

}
