package com.kshrd.klearing.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class TypeResponse {
    @NotNull(message = "ID can not be null")
    private int id;
    @NotEmpty(message = "Name can not be empty")
    @NotBlank(message = "Name can not be blank")
    private String name;
}
