package com.kshrd.klearing.model.response;


import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Set;

@Data
public class JwtResponseModel {
    private final String message;
    private final String jwtToken;
    private Collection<? extends GrantedAuthority> role;

    public JwtResponseModel(String message, String jwtToken, Collection<? extends GrantedAuthority> role) {
        this.message = message;
        this.jwtToken = jwtToken;
        this.role = role;
    }

    public JwtResponseModel(String message, String jwtToken) {
        this.message = message;
        this.jwtToken = jwtToken;
    }
}
