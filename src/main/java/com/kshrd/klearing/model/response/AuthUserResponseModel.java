package com.kshrd.klearing.model.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.security.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthUserResponseModel {
    private String name;
    private String username;
    private String gender;
    private String email;
    private Timestamp registerDate;
    private boolean status;
    private List<String> roles;
}
