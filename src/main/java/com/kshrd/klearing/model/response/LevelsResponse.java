package com.kshrd.klearing.model.response;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LevelsResponse {
    @ApiModelProperty(position = 1)
    private Integer id;
    @ApiModelProperty(position = 2)
    private String name;
    @ApiModelProperty(position = 3)
    private Integer visibility;
    @ApiModelProperty(position = 4)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd@HH:mm:ssZ")
    private String createdDate;
    @ApiModelProperty(position = 5)
    private Integer userID;
}
