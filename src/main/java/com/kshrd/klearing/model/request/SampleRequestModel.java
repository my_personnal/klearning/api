package com.kshrd.klearing.model.request;

import io.swagger.annotations.ApiModelProperty;

public class SampleRequestModel {
        @ApiModelProperty(value = "The type", position = -1, required = true)
        private String type;

        @ApiModelProperty(hidden = true)
        private Class<UserRequest> clazz;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @ApiModelProperty(hidden = true)
        public Class<UserRequest> getClazz() {
            return clazz;
        }

        @ApiModelProperty(hidden = true)
        public void setClazz(Class<UserRequest> clazz) {
            this.clazz = clazz;
        }
}
