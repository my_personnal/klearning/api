package com.kshrd.klearing.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubTypeRequest {
    @NotEmpty(message = "Subtype name can not be empty")
    @NotBlank(message = "Subtype name can not be blank")
    private String subType;

}
