package com.kshrd.klearing.model.request.reward;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class RewardRequest {
    @ApiModelProperty(position = 1)
    @NotBlank(message = "Name can not be blank")
    @NotEmpty(message = "Name can not be empty")
    String name;
    @ApiModelProperty(position = 2)
    List<String> content;
    @ApiModelProperty
    @JsonIgnore
    int userID;
}
