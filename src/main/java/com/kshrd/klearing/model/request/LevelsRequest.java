package com.kshrd.klearing.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.sql.Date;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class LevelsRequest {
    @ApiModelProperty(position = 1)
    @NotEmpty(message = "Name can not be empty")
    @NotBlank(message = "Name can not be blank")
    private String name;
    @JsonIgnore
    @ApiModelProperty(position = 2)
    private Integer visibility;
    @JsonIgnore
    @ApiModelProperty(position = 3)
    private LocalDateTime createdDate;
    @ApiModelProperty(position = 4)
    @JsonIgnore
    private Integer userID;
}
