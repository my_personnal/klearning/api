package com.kshrd.klearing.model.request;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@ApiModel
@Data
public class JwtRequestModel {
    @ApiModelProperty(position = 1)
    @NotEmpty(message = "username can not be empty")
    @NotBlank(message = "username can not be blank")
    private String username;
    @ApiModelProperty(position = 2)
    @NotEmpty(message = "password can not be empty")
    @NotBlank(message = "password can not be blank")
    private String password;;

    public JwtRequestModel()
    {
    }

    public JwtRequestModel(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password; }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "JwtRequestDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
