package com.kshrd.klearing.model.request;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateLevelRequest {
    @ApiModelProperty(position = 1)
    private Integer id;
    @ApiModelProperty(position = 2)
    private String name;
    @JsonIgnore
    @ApiModelProperty(position = 3)
    private Integer visibility;
    @JsonIgnore
    @ApiModelProperty(position = 4)
    private LocalDateTime createdDate;
    @JsonIgnore
    @ApiModelProperty(position = 5)
    private Integer userID;
}
