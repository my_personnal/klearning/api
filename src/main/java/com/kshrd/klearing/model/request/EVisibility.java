package com.kshrd.klearing.model.request;


public enum EVisibility {
    Private,
    Public,
    Group
}
