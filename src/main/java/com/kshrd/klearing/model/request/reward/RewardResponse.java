package com.kshrd.klearing.model.request.reward;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RewardResponse {
    private Integer id;
    private String name;
    private List<String> content;
    private Integer userID;
}
