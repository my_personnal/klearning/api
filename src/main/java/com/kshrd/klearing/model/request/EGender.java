package com.kshrd.klearing.model.request;

public enum EGender {
    Male,
    Female
}
