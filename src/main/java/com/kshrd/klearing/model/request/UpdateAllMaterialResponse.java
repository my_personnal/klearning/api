package com.kshrd.klearing.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kshrd.klearing.model.materials.content.Content;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateAllMaterialResponse {

    Integer userId;
    @ApiModelProperty(required=true, position = 1)
    private Integer id;
    @ApiModelProperty(required=true, position = 2)
    private String name;
    @ApiModelProperty(required=true, position = 3)
    private String description;
    @ApiModelProperty(required=true, position = 4)
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd@HH:mm:ssZ")
    private String createdDate;
    @ApiModelProperty(required=true, position = 5)
    private Content content;
    @ApiModelProperty(required=true, position = 6)
    private Integer typeID;
    @ApiModelProperty(required=true, position = 7)
    private Integer levelID;
    @ApiModelProperty(required=true, position = 8)
    private Integer status;
    @ApiModelProperty(required=true, position = 9)
    private Integer count;
    @ApiModelProperty(required=true, position = 10)
    private Integer subTypeID;
}
