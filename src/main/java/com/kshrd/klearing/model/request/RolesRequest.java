package com.kshrd.klearing.model.request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class RolesRequest {
    @NotEmpty(message = "Role's Name can not empty ")
    @NotBlank(message = "Role's Name can not be blank")
    @Size(min=4, max = 15)
    private String name;

}
