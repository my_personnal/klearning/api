package com.kshrd.klearing.model.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class UpdateUserRequest {
    @ApiModelProperty(required=true, position = 1)
    @NotNull(message = "ID can not be null")
    private Integer id;
    @ApiModelProperty(position = 2)
    @NotEmpty(message = "Name can not be empty")
    @NotBlank(message = "Name can not be blank")
    private String name;
    @ApiModelProperty(position = 3)
    @NotEmpty(message = "Username can not be empty")
    @NotBlank(message = "Username can not be blank")
    @Pattern(regexp = "^[a-z0-9]+[-_]*[a-z0-9]*$", message = "Username contain characters lowercase only and not contain whitespace")
    private String username;
    @ApiModelProperty(position = 4)
    @JsonIgnore
    private EGender gender;
    @ApiModelProperty(position = 5)
    @Email(message = "Email is not valid")
    @NotBlank(message = "email can not be blank")
    @NotEmpty(message = "email can not be empty")
    private String email;
    @ApiModelProperty(position = 6)
    @JsonIgnore
    private Integer roleID;
    @ApiModelProperty(position = 7)
    @NotNull(message = "Status can not be null")
    private Boolean status;
    @JsonIgnore
    @ApiModelProperty(position = 8)
    private LocalDateTime registerDate;
    @ApiModelProperty(position = 9)
    private String image;
}