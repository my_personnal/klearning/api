package com.kshrd.klearing.model.request;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthUserRequestModel {
    private String name;
    private String username;
    private String gender;
    private String password;
    @Email(message = "invalid email")
    private String email;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd@HH:mm:ss")
    private String registerDate;
    private int roleId;
    private boolean status;
}
