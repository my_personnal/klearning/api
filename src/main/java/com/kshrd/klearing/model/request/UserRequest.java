package com.kshrd.klearing.model.request;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class UserRequest {
    @ApiModelProperty(position = 1)
    @NotEmpty(message = "Name can not be empty")
    @NotBlank(message = "Name can not be blank")
    private String name;
    @NotEmpty(message = "Username can not be empty")
    @NotBlank(message = "Username can not be blank")
    @ApiModelProperty(position = 2)
    @Pattern(regexp = "^[a-z0-9]+[-_]*[a-z0-9]*$", message = "Username contain characters lowercase only and not contain whitespace")
    private String username;
    @ApiModelProperty(position = 3)
    @JsonIgnore
    private EGender gender;
    @ApiModelProperty(position = 4)
    @Email(message = "Email is not valid")
    @NotBlank(message = "email can not be blank")
    @NotEmpty(message = "email can not be empty")
    private String email;
    @ApiModelProperty(position = 5)
    @NotEmpty(message = "Password can not be empty")
    @NotBlank(message = "Password can not be blank")
    @Size(min = 6,max = 15, message = "Passwords must be at least six characters and less than 15 characters")
    private String password;
    @ApiModelProperty(position = 6)
    @JsonIgnore
    private LocalDateTime registerDate;
    @JsonIgnore
    private int role_id;
    @ApiModelProperty(position = 7)
    @NotNull(message = "Status can not be null")
    private Boolean status;
    @ApiModelProperty(position = 8)
    private String image;
}
