package com.kshrd.klearing.model.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kshrd.klearing.model.materials.content.Content;
import com.kshrd.klearing.model.materials.content.contentforvocab.ContentForVoCab;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateAllMaterialRequest {
    @JsonIgnore
    Integer userId;
    @ApiModelProperty(required=true, position = 1)
    private Integer id;
    @ApiModelProperty(required=true, position = 2)
    @NotEmpty(message = "Name can not be empty")
    @NotBlank(message = "Name can not be blank")
    private String name;
    @ApiModelProperty(required=true, position = 3)
    @NotNull(message = "Description can not be NotBlank")
    private String description;
    @ApiModelProperty(required=true, position = 4)
    @JsonIgnore
    private LocalDateTime createdDate;
    @ApiModelProperty(required=true, position = 5)
    private Content content;
    @ApiModelProperty(required=true, position = 6)
    private Integer typeID;
    @ApiModelProperty(required=true, position = 7)
    private Integer levelID;
    @JsonIgnore
    @ApiModelProperty(required=true, position = 8)
    private Integer status;
    @ApiModelProperty(required=true, position = 10)
    private Integer subTypeID;
}
