package com.kshrd.klearing.model.result;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAnswer {
    @ApiModelProperty(position = 1)
    private String question;
    @ApiModelProperty(position = 2)
    private List<String> answer;
}
