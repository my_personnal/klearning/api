package com.kshrd.klearing.model.result;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultResponse {
    @ApiModelProperty(required=true,position = 1)
    private Integer id;
    @ApiModelProperty(required=true,position = 2)
    private List<UserAnswer> userAnswers;
    @ApiModelProperty(required=true,position = 3)
    private Integer totalScore;
    @ApiModelProperty(required=true,position = 4)
    private Integer userScore;
    @ApiModelProperty(required=true,position = 5)
    private Integer materialID;
    @ApiModelProperty(required=true,position = 6)
    private Integer userID;
}
