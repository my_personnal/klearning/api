package com.kshrd.klearing.model.materials;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeleteResponse {
    private Integer id;
    private String name;
}
