package com.kshrd.klearing.model.materials.content.contentforvocab;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Content {
    @ApiModelProperty(required=true, position = 1)
    private String titleVocab;
    @ApiModelProperty(required=true, position = 2)
    private ItemVocabForConsonant item;
}
