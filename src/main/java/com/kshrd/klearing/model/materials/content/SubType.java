package com.kshrd.klearing.model.materials.content;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SubType {
    private Integer id;
    private String subType;
}
