package com.kshrd.klearing.model.materials.content;
import com.kshrd.klearing.model.materials.content.subcontent.Alphabet.MaterialElement;
import com.kshrd.klearing.model.materials.content.subcontent.FileContent;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Content {
    AllMaterialElement allMaterialElements;
}
