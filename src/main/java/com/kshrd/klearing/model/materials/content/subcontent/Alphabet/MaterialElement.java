package com.kshrd.klearing.model.materials.content.subcontent.Alphabet;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MaterialElement {
    @ApiModelProperty(required=true, position = 1)
    private String title;
    @ApiModelProperty(required=true, position = 2)
    private List<Items> items;
}
