package com.kshrd.klearing.model.materials.content.practice;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FillingTheBlankItem {
    private String question;
    private String answer;
}
