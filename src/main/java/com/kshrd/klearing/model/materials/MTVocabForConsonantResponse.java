package com.kshrd.klearing.model.materials;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.kshrd.klearing.model.materials.content.contentforvocab.ContentForVoCab;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MTVocabForConsonantResponse {
    private Integer id;
    private String name;
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "yyyy-MM-dd@HH:mm:ssZ")
    private String createdDate;
    private ContentForVoCab content;
    private Integer typeID;
    private Integer levelID;
    private Integer status;
    private Integer count ;
    private Integer subTypeID;
}
