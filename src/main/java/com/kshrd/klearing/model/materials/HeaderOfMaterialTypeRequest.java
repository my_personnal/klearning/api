package com.kshrd.klearing.model.materials;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kshrd.klearing.model.materials.content.contentforvocab.ContentForVoCab;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class HeaderOfMaterialTypeRequest {
    @NotEmpty(message = "Name can not be empty")
    @NotBlank(message = "Name can not be blank")
    @ApiModelProperty(position = 1)
    private String name;
    @NotNull(message = "Description can not be NotBlank")
    @ApiModelProperty(position = 2)
    private String description;
    @JsonIgnore
    private LocalDateTime createdDate;
    @ApiModelProperty(position = 3)
    private Integer typeID;
    @JsonIgnore
    private Integer userID;
    @JsonIgnore
    private Integer  status;
    @ApiModelProperty(position = 4)
    private Integer subTypeID;
    @ApiModelProperty(position = 5)
    private Integer LevelID;

}
