package com.kshrd.klearing.model.materials;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kshrd.klearing.model.materials.content.contentforvocab.ContentForVoCab;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MTVocabForConsonantRequest {
    @ApiModelProperty(required=true, position = 1)
    @NotEmpty(message = "Name can not be empty")
    @NotBlank(message = "Name can not be blank")
    private String name;
    @ApiModelProperty(required=true, position = 2)
    @NotNull(message = "Description can not be NotBlank")
    private String description;
    @JsonIgnore
    @ApiModelProperty(required=true, position = 3)
    private LocalDateTime createdDate;
    @ApiModelProperty(required=true, position = 4)
    private ContentForVoCab content;
    @ApiModelProperty(required=true, position = 5)
    private Integer typeID;
    @JsonIgnore
    @ApiModelProperty(required=true, position = 5)
    private Integer userID;
    @ApiModelProperty(required=true, position = 6)
    private Integer levelID;
    @JsonIgnore
    @ApiModelProperty(required=true, position = 7)
    private Integer status;
    @ApiModelProperty(required=true, position = 8)
    private Integer subTypeID;
}
