package com.kshrd.klearing.model.materials.content.subcontent;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileContent {
    @ApiModelProperty(required=true, position = 1)
    private String fileUpload;
    @ApiModelProperty(required=true, position = 2)
    private String description;
}
