package com.kshrd.klearing.model.materials.content.subcontent.Alphabet;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Items {
    @ApiModelProperty(required=true, position = 1)
    private String korean;
    @ApiModelProperty(required=true, position = 2)
    private String khmer;
    @ApiModelProperty(required=true, position = 3)
    private String pronoun;
}
