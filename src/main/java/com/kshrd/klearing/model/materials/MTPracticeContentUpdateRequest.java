package com.kshrd.klearing.model.materials;
import com.kshrd.klearing.model.materials.content.practice.PracticeContent;
import com.kshrd.klearing.model.materials.content.practice.PracticeFillingTheBlank;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MTPracticeContentUpdateRequest {

    @ApiModelProperty(required=true, position = 1)
    private Integer id;
    @ApiModelProperty(required=true, position = 5)
    private PracticeContent content;

}
