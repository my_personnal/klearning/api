package com.kshrd.klearing.model.materials.content.contentforvocab;
import com.kshrd.klearing.model.materials.content.subcontent.Alphabet.Items;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemVocabForConsonant{
    @ApiModelProperty(required=true, position = 1)
    private String korean;
    @ApiModelProperty(required=true, position = 2)
    private String khmer;
    @ApiModelProperty(required=true, position = 3)
    private String pronoun;
    @ApiModelProperty(required=true, position = 4)
    private String image;

}
