package com.kshrd.klearing.model.materials;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kshrd.klearing.model.materials.content.practice.PracticeFillingTheBlank;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MTAllPracticeFillingTheBlankUpdateRequest {
    @JsonIgnore
    Integer userId;
    @ApiModelProperty(required=true, position = 1)
    private Integer id;
    @ApiModelProperty(required=true, position = 2)
    @NotEmpty(message = "Name can not be empty")
    @NotBlank(message = "Name can not be blank")
    private String name;
    @ApiModelProperty(required=true, position = 3)
    @NotEmpty(message = "Description can not be empty")
    private String description;
    @ApiModelProperty(required=true, position = 4)
    @JsonIgnore
    private LocalDateTime createdDate;
    @ApiModelProperty(required=true, position = 5)
    private PracticeFillingTheBlank content;
    @ApiModelProperty(required=true, position = 6)
    private Integer typeID;
    @ApiModelProperty(required=true, position = 7)
    private Integer levelID;
    @ApiModelProperty(required=true, position = 8)
    @JsonIgnore
    private Integer status;
    @ApiModelProperty(required=true, position = 10)
    private Integer subTypeID;

}
