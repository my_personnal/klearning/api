package com.kshrd.klearing.model.materials.content.subcontent.Alphabet;
import com.kshrd.klearing.model.materials.content.subcontent.FileContent;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Alphabets {
    @ApiModelProperty(required=true, position = 1)
    private String title;
    @ApiModelProperty(required=true, position = 3)
    private List<MaterialElement> materialElements;
    @ApiModelProperty(required=true, position = 4)
    private FileContent imageDescriptions;
}
