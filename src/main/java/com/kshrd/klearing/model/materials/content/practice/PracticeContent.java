package com.kshrd.klearing.model.materials.content.practice;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PracticeContent {
    PracticeItem practiceItems;
}
