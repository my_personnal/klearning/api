package com.kshrd.klearing.model.enums;

import com.kshrd.klearing.model.PropertiesDynaEnum;
import com.kshrd.klearing.repository.PracticeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public enum Data {
    A(), B(), C();

    List<PropertiesDynaEnum> levels;
    @Autowired
    LevelTypeConfiguration levelTypeConfiguration;
    Data() {
       levelTypeConfiguration = new LevelTypeConfiguration();
        levels = levelTypeConfiguration.level();

    }

    public List<PropertiesDynaEnum> getLevels() {
        return levels;
    }
}