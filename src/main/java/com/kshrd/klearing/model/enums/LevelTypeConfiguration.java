package com.kshrd.klearing.model.enums;

import com.kshrd.klearing.model.PropertiesDynaEnum;
import com.kshrd.klearing.repository.PracticeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;


public class LevelTypeConfiguration {
    @Autowired
    PracticeRepository practiceRepository;


    List<PropertiesDynaEnum> level(){
        List<PropertiesDynaEnum> propertiesDynaEnums = new ArrayList<>();
        PropertiesDynaEnum dynaEnum ;
        List<Integer> level = practiceRepository.getAllLevel();
        for(int i=0;i<level.size();i++){
            dynaEnum = new PropertiesDynaEnum(level.get(i).toString(),i);
            propertiesDynaEnums.add(dynaEnum);
        }
        return propertiesDynaEnums;
    }


}
