package com.kshrd.klearing.service.servieImp;
import com.kshrd.klearing.model.request.UpdateUserRequest;
import com.kshrd.klearing.model.request.UserRequest;
import com.kshrd.klearing.model.response.UserResponse;
import com.kshrd.klearing.repository.UserRepository;
import com.kshrd.klearing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {


    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;


    public UserServiceImp(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }
    @Override
    public List<UserResponse> getAllUsers() {
        List<UserResponse> userResponseList = userRepository.getAllUsers();
        System.out.println(userResponseList);
        return userResponseList;
    }
    @Override
    public List<UserResponse> findUserByName(String name) {
        return userRepository.findUserByName(name);
    }

    @Override
    public String deleteUserById(Integer userId) {
        return userRepository.deleteUserByStatus(userId);
    }

    @Override
    public UserResponse insertUser(UserRequest userRequest) {
        String encryptedPassword = passwordEncoder.encode(userRequest.getPassword());
        userRequest.setPassword(encryptedPassword);
        return userRepository.insertUser(userRequest);
    }

    @Override
    public UserResponse updateUserById(UpdateUserRequest updateUserRequest) {
        return userRepository.updateUserById(updateUserRequest);
    }

}
