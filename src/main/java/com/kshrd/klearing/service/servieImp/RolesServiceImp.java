package com.kshrd.klearing.service.servieImp;

import com.kshrd.klearing.model.request.RolesRequest;
import com.kshrd.klearing.model.response.RolesResponse;
import com.kshrd.klearing.repository.RolesRepository;
import com.kshrd.klearing.service.RolesService;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolesServiceImp implements RolesService {

    @Autowired
    private RolesRepository rolesRepository;

    @Override
    public List<RolesResponse> findAllRoles() {
        return rolesRepository.findAllRoles() ;
    }

    @Override
    public List<RolesResponse> findRoleByName(String rolesRequest) {
        return rolesRepository.findRoleByName(rolesRequest);
    }

    @Override
    public RolesResponse insertRoles(RolesRequest rolesRequest) {
        return rolesRepository.insertRoles(rolesRequest);
    }

    @Override
    public void removeRolesById(Integer id) throws NotFoundException {

    }

    @Override
    public RolesResponse updateRolesByName(RolesResponse roleName){
        return rolesRepository.updateRoleByName(roleName);
    }

    @Override
    public String deleteRoleById(String roleName) {
        return rolesRepository.deleteRoleById(roleName);
    }
}
