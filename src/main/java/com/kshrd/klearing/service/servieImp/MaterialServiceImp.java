package com.kshrd.klearing.service.servieImp;
import com.kshrd.klearing.model.materials.*;
import com.kshrd.klearing.model.materials.content.Content;
import com.kshrd.klearing.model.materials.MTPracticeFillingTheBlankRequest;
import com.kshrd.klearing.model.materials.content.SubType;
import com.kshrd.klearing.model.materials.content.practice.PracticeContent;
import com.kshrd.klearing.model.materials.content.practice.PracticeFillingTheBlank;
import com.kshrd.klearing.model.request.UpdateAllMaterialRequest;
import com.kshrd.klearing.repository.MaterialRepository;
import com.kshrd.klearing.service.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialServiceImp implements MaterialService {

    @Autowired
    MaterialRepository materialRepository;

    @Override
    public MaterialResponse InsertMaterial(MaterialRequest materialRequest) {
        return materialRepository.insertMaterial(materialRequest);
    }

    @Override
    public MaterialUpdateRequest updateMaterialContent(MaterialUpdateRequest materialUpdateRequest) {
        return materialRepository.updateMaterialContentById(materialUpdateRequest);
    }

    @Override
    public MTVocabForConsonantResponse updateAllVocabForConsonantById(UpdateAllMTVocabForConsonantRequest updateMTVocabForConsonantRequest) {
        return materialRepository.updateAllVocabForConsonantById(updateMTVocabForConsonantRequest);
    }

    @Override
    public HeaderOfMaterialTypeResponse InsertHeaderOfMaterialType(HeaderOfMaterialTypeRequest materialRequest) {
        return materialRepository.InsertHeaderOfMaterialType(materialRequest);
    }

    @Override
    public MTVocabForConsonantResponse InsertMaterial(MTVocabForConsonantRequest mtVocabForConsonantRequest) {
        return materialRepository.insertMTVocabForConsonant(mtVocabForConsonantRequest);
    }

    @Override
    public List<SubType> getSubTypeByLevelIdAndTypeId(Integer levelId, Integer typeId) {
        return materialRepository.getSubTypeByLevelIdAndTypeId(levelId,typeId);
    }

    @Override
    public List<Content> getContentByTypeIDLevelIdAndSubTypeId(Integer levelId,Integer typeId, Integer subtypeId) {
        return materialRepository.getContentByTypeIDLevelIdAndSubTypeId( levelId, typeId,  subtypeId);
    }

    @Override
    public List<MaterialResponse> getAllMaterials(Integer levelID,Integer subtypeID, Integer typeID) {
        return materialRepository.getAllMaterial(levelID, subtypeID, typeID);
    }

    @Override
    public List<MTVocabForConsonantResponse> getAllVocabForConsonant(Integer levelID, Integer subtypeID, Integer typeID) {
        return materialRepository.getAllVocabForConsonant(levelID,subtypeID,typeID);
    }

    @Override
    public List<MaterialInfoTypeResponse> getMaterialInfo(Integer levelID, Integer typeID) {
        return materialRepository.getMaterialInfo(levelID,typeID);
    }

    @Override
    public List<PracticeContent> getPracticeContentByTypeIDLevelIdAndSubTypeId(Integer levelId, Integer typeId, Integer subtypeId) {
        return materialRepository.getPracticeContentByTypeIDLevelIdAndSubTypeId(levelId,typeId,subtypeId);
    }

    @Override
    public MTPracticeResponse insertPracticeContent(MTPracticeRequest mtPracticeRequest) {
        return materialRepository.insertPracticeContent(mtPracticeRequest);
    }

    @Override
    public MTPracticeFillingTheBlankResponse insertPracticeFillingTheBlankContent(MTPracticeFillingTheBlankRequest mtPracticeFillingTheBlankRequest) {
        return materialRepository.mtPracticeFillingTheBlankRequest(mtPracticeFillingTheBlankRequest);
    }

    @Override
    public List<PracticeFillingTheBlank> getPracticeFillingTheBlankContentByTypeIDLevelIdAndSubTypeId(Integer levelId, Integer subtypeId, Integer typeId) {
        return materialRepository.getPracticeFillingTheBlankContentByTypeIDLevelIdAndSubTypeId(levelId,subtypeId,typeId);
    }

    @Override
    public MTPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest(MTPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest) {
        return materialRepository.mtPracticeFillingTheBlankUpdateRequest(mtPracticeFillingTheBlankUpdateRequest);
    }

    @Override
    public MTPracticeContentUpdateRequest UpdatePracticeContentByID(MTPracticeContentUpdateRequest mtPracticeContentUpdateRequest) {
        return materialRepository.UpdatePracticeContentByID(mtPracticeContentUpdateRequest);
    }

    @Override
    public DeleteResponse deleteMaterialById(Integer deleteMaterialById) {
        return materialRepository.deleteMaterialById(deleteMaterialById);
    }

    @Override
    public UpdateAllMaterialRequest UpdateMaterialById(UpdateAllMaterialRequest updateAllMaterialRequest) {
        return materialRepository.updateMaterialById(updateAllMaterialRequest);
    }

    @Override
    public UpdateMTVocabForConsonantResponse updateVocabForConsonantById(UpdateMTVocabForConsonantRequest updateVocabForConsonantById) {
        return materialRepository.updateVocabForConsonantById(updateVocabForConsonantById);
    }

    @Override
    public MTAllPracticeFillingTheBlankUpdateResponse UpdateAllPracticeContentByID(MTAllPracticeContentUpdateRequest mtAllPracticeContentUpdateRequest) {
        return materialRepository.mtAllPracticeContentUpdateRequest(mtAllPracticeContentUpdateRequest);
    }

    @Override
    public MTAllPracticeFillingTheBlankUpdateResponse mtAllPracticeFillingTheBlankUpdateRequest(MTAllPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest) {
        return materialRepository.mtAllPracticeFillingTheBlankUpdateRequest(mtPracticeFillingTheBlankUpdateRequest);
    }

}
