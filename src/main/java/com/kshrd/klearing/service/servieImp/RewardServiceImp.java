package com.kshrd.klearing.service.servieImp;

import com.kshrd.klearing.model.request.reward.RewardRequest;
import com.kshrd.klearing.model.request.reward.RewardResponse;
import com.kshrd.klearing.model.request.reward.RewardUpdateRequest;
import com.kshrd.klearing.repository.RewardRepository;
import com.kshrd.klearing.service.RewardService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RewardServiceImp implements RewardService {


    @Autowired
    RewardRepository rewardRepository;
    @Override
    public RewardResponse insertReward(RewardRequest rewardRequest) {
        return rewardRepository.insertReward(rewardRequest);
    }

    @Override
    public RewardResponse updateRewardByID(RewardUpdateRequest updateRewardByID) {
        return rewardRepository.updateRewardByID(updateRewardByID);
    }

    @Override
    public List<RewardResponse> getRewardByName(String name) {
        return rewardRepository.getRewardByName(name);
    }

    @Override
    public RewardResponse deleteRewardByID(Integer id) {
        return rewardRepository.deleteRewardByID(id);
    }

    @Override
    public List<RewardResponse> getAllReward() {
        return rewardRepository.getAllReward();
    }
}
