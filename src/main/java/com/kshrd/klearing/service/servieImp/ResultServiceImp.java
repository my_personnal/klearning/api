package com.kshrd.klearing.service.servieImp;
import com.kshrd.klearing.model.result.ResultRequest;
import com.kshrd.klearing.model.result.ResultResponse;
import com.kshrd.klearing.model.result.ResultUpdateRequest;
import com.kshrd.klearing.repository.ResultRepository;
import com.kshrd.klearing.service.ResultsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultServiceImp implements ResultsService {

    @Autowired
    ResultRepository resultRepository;

    @Override
    public List<ResultResponse> findAllResults() {
        return resultRepository.getAllResults();
    }

    @Override
    public List<ResultResponse> findLevelByName(Integer id) {
        return resultRepository.findAllResultByName(id);
    }

    @Override
    public ResultResponse insertResult(ResultRequest resultRequest) {
        return resultRepository.insertResult(resultRequest);
    }

    @Override
    public ResultResponse updateResultByID(ResultUpdateRequest response) {
        return resultRepository.updateResultById(response);
    }

    @Override
    public ResultResponse deleteResultsByID(Integer id) {
        return resultRepository.deleteResultByID(id);
    }
}
