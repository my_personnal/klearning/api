package com.kshrd.klearing.service.servieImp;
import com.kshrd.klearing.model.FileStorageProperties;
import com.kshrd.klearing.service.FileStorageService;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageServiceImp extends FileStorageService {

    public FileStorageServiceImp(FileStorageProperties fileStorageProperties) {
        super(fileStorageProperties);
    }

    @Override
    public String storeFile(MultipartFile file) {
        return null;
    }

    @Override
    public Resource loadFileAsResource(String fileName) {
        return null;
    }
}

