package com.kshrd.klearing.service.servieImp;

import com.kshrd.klearing.model.request.LevelsRequest;
import com.kshrd.klearing.model.request.UpdateLevelRequest;
import com.kshrd.klearing.model.response.LevelsResponse;
import com.kshrd.klearing.repository.LevelsRepository;
import com.kshrd.klearing.service.LevelsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LevelsServiceImp implements LevelsService {

    @Autowired
    LevelsRepository levelsRepository;
    @Override
    public List<LevelsResponse> findAllLevels() {
        return levelsRepository.findAllLevels();
    }

    @Override
    public List<LevelsResponse> findLevelByName(String name) {
        return levelsRepository.findLevelByName(name);
    }

    @Override
    public LevelsResponse insertLevel(LevelsRequest levelsRequest) {
        return levelsRepository.insertLevel(levelsRequest);
    }

    @Override
    public LevelsResponse updateLevelByName(UpdateLevelRequest levelsResponse) {
        return levelsRepository.updateLevelByName(levelsResponse);
    }

    @Override
    public Integer deleteLevelById(Integer id) {
        return levelsRepository.deleteLevelById(id);
    }
}
