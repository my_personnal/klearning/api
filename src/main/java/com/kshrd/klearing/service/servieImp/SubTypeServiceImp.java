package com.kshrd.klearing.service.servieImp;
import com.kshrd.klearing.model.request.SubTypeRequest;
import com.kshrd.klearing.model.response.SubTypeResponse;
import com.kshrd.klearing.repository.SubtypeRepository;
import com.kshrd.klearing.service.SubTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubTypeServiceImp implements SubTypeService {

    @Autowired
    SubtypeRepository subtypeRepository;

    @Override
    public List<SubTypeResponse> findAllSubTypes() {
        System.out.println(subtypeRepository.findAllNameSubTypes());
        return subtypeRepository.findAllSubTypes() ;
    }

    @Override
    public List<SubTypeResponse> findSubTypeByName(String name) {
        return subtypeRepository.findSubTypeByName(name);
    }

    @Override
    public SubTypeResponse insertSubType(SubTypeRequest subTypeRequest) {
        return subtypeRepository.insertSubType(subTypeRequest);
    }

    @Override
    public SubTypeResponse updateSubTypeByID(SubTypeResponse subTypeResponse) {
        return subtypeRepository.updateSubTypeByID(subTypeResponse);
    }

    @Override
    public Integer deleteSubTypeByID(Integer subTypeID) {
        return subtypeRepository.deleteSubTypeByName(subTypeID);
    }
}
