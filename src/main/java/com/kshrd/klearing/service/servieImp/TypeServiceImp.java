package com.kshrd.klearing.service.servieImp;

import com.kshrd.klearing.model.request.TypeRequest;
import com.kshrd.klearing.model.response.TypeResponse;
import com.kshrd.klearing.repository.TypeRepository;
import com.kshrd.klearing.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TypeServiceImp implements TypeService {

    @Autowired
    TypeRepository typeRepository;

    @Override
    public List<TypeResponse> findAllType() {
        return typeRepository.findAllTypes();
    }

    @Override
    public List<TypeResponse> findTypeByName(String name) {
        return typeRepository.findTypeByName(name);
    }

    @Override
    public TypeResponse insertType(TypeRequest typeRequest) {
        return typeRepository.insertType(typeRequest);
    }

    @Override
    public String deleteTypeByName(String name) {
       return typeRepository.deleteTypeByName(name);
    }

    @Override
    public TypeResponse updateTypeByName(TypeResponse typeResponse) {
        return typeRepository.updateTypeByName(typeResponse);
    }

}
