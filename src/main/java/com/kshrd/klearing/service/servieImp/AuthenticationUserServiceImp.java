package com.kshrd.klearing.service.servieImp;

import com.kshrd.klearing.model.AuthenticationUsers;
import com.kshrd.klearing.model.request.AuthUserRequestModel;
import com.kshrd.klearing.model.response.AuthUserResponseModel;
import com.kshrd.klearing.repository.AuthenticationUserRepository;
import com.kshrd.klearing.service.AuthenticationUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthenticationUserServiceImp implements AuthenticationUserService {

    @Autowired
    private AuthenticationUserRepository userRepository;

    @Override
    public Boolean insertUser(AuthUserRequestModel user, int user_role) {
        return null;
    }

    @Override
    public List<AuthUserResponseModel> findUserAll() {
        return null;
    }

    @Override
    public AuthUserResponseModel findUserId(int id) {
        return null;
    }

    @Override
    public Boolean updateUser(int id, AuthenticationUsers user) {
        return null;
    }

    @Override
    public Boolean deleteUser(int id) {
        return null;
    }

    @Override
    public List<String> getUsername() {
        return userRepository.getUsername();
    }

    @Override
    public Boolean userStatus(String username) {
        return userRepository.getStatus(username);
    }

    @Override
    public int selectUserID(String username) {
        return userRepository.selectUserID(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("User repo");
        AuthenticationUsers users = null;
        try {
              users = userRepository.loadUser(username);
            System.out.println("User is:"+users.toString());
            if(users == null)
                throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
            else {
                return users;
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return users;
    }
}
