package com.kshrd.klearing.service;
import com.kshrd.klearing.model.request.RolesRequest;
import com.kshrd.klearing.model.response.RolesResponse;
import io.swagger.models.auth.In;
import org.apache.ibatis.javassist.NotFoundException;

import java.util.List;

public interface RolesService {

    List<RolesResponse> findAllRoles();

    List<RolesResponse> findRoleByName(String rolesRequest);

    RolesResponse insertRoles(RolesRequest rolesRequest);

    void removeRolesById(Integer id) throws NotFoundException;

    RolesResponse updateRolesByName(RolesResponse roleName);


    String deleteRoleById(String roleName);
}
