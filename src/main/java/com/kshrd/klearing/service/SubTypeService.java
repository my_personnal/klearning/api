package com.kshrd.klearing.service;


import com.kshrd.klearing.model.request.SubTypeRequest;
import com.kshrd.klearing.model.response.SubTypeResponse;

import java.util.List;

public interface SubTypeService {

    List<SubTypeResponse> findAllSubTypes();

    List<SubTypeResponse> findSubTypeByName(String name);

    SubTypeResponse insertSubType(SubTypeRequest subTypeRequest);

    SubTypeResponse updateSubTypeByID(SubTypeResponse subTypeResponse);

    Integer deleteSubTypeByID(Integer subTypeID);
}
