package com.kshrd.klearing.service;


import com.kshrd.klearing.model.materials.*;
import com.kshrd.klearing.model.materials.content.Content;
import com.kshrd.klearing.model.materials.MTPracticeFillingTheBlankRequest;
import com.kshrd.klearing.model.materials.content.SubType;
import com.kshrd.klearing.model.materials.content.practice.PracticeContent;
import com.kshrd.klearing.model.materials.content.practice.PracticeFillingTheBlank;
import com.kshrd.klearing.model.request.UpdateAllMaterialRequest;

import java.util.List;

public interface MaterialService {

    MaterialResponse InsertMaterial (MaterialRequest materialRequest);

    MaterialUpdateRequest updateMaterialContent (MaterialUpdateRequest materialUpdateRequest);

    MTVocabForConsonantResponse updateAllVocabForConsonantById (UpdateAllMTVocabForConsonantRequest updateMTVocabForConsonantRequest);

    HeaderOfMaterialTypeResponse InsertHeaderOfMaterialType (HeaderOfMaterialTypeRequest materialRequest);

    MTVocabForConsonantResponse InsertMaterial (MTVocabForConsonantRequest mtVocabForConsonantRequest);

    List<SubType> getSubTypeByLevelIdAndTypeId(Integer levelId, Integer typeId);
    List<Content> getContentByTypeIDLevelIdAndSubTypeId(Integer levelId,Integer typeId, Integer subtypeId);
    List<MaterialResponse> getAllMaterials(Integer levelID,Integer subtypeID, Integer typeID);

    List<MTVocabForConsonantResponse> getAllVocabForConsonant(Integer levelID, Integer subtypeID, Integer typeID);


    List<MaterialInfoTypeResponse> getMaterialInfo(Integer levelID, Integer typeID);

    List<PracticeContent> getPracticeContentByTypeIDLevelIdAndSubTypeId(Integer levelId, Integer typeId, Integer subtypeId);

    MTPracticeResponse insertPracticeContent(MTPracticeRequest mtPracticeRequest);

    MTPracticeFillingTheBlankResponse insertPracticeFillingTheBlankContent(MTPracticeFillingTheBlankRequest mtPracticeFillingTheBlankRequest);

    List<PracticeFillingTheBlank> getPracticeFillingTheBlankContentByTypeIDLevelIdAndSubTypeId(Integer levelId, Integer subtypeId, Integer typeId);

    MTPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest(MTPracticeFillingTheBlankUpdateRequest MTPracticeFillingTheBlankUpdateRequest);

    MTPracticeContentUpdateRequest UpdatePracticeContentByID(MTPracticeContentUpdateRequest mtPracticeContentUpdateRequest);

    DeleteResponse deleteMaterialById(Integer deleteMaterialById);

    UpdateAllMaterialRequest UpdateMaterialById(UpdateAllMaterialRequest updateAllMaterialRequest);

    UpdateMTVocabForConsonantResponse updateVocabForConsonantById(UpdateMTVocabForConsonantRequest updateVocabForConsonantById);

    MTAllPracticeFillingTheBlankUpdateResponse UpdateAllPracticeContentByID(MTAllPracticeContentUpdateRequest mtAllPracticeContentUpdateRequest);

    MTAllPracticeFillingTheBlankUpdateResponse mtAllPracticeFillingTheBlankUpdateRequest(MTAllPracticeFillingTheBlankUpdateRequest mtPracticeFillingTheBlankUpdateRequest);
}
