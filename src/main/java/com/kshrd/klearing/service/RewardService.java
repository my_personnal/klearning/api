package com.kshrd.klearing.service;

import com.kshrd.klearing.model.request.reward.RewardRequest;
import com.kshrd.klearing.model.request.reward.RewardResponse;
import com.kshrd.klearing.model.request.reward.RewardUpdateRequest;

import java.util.List;

public interface RewardService {


    RewardResponse insertReward(RewardRequest rewardRequest);

    RewardResponse updateRewardByID(RewardUpdateRequest updateRewardByID);

    List<RewardResponse> getRewardByName(String name);

    RewardResponse deleteRewardByID(Integer id);

    List<RewardResponse> getAllReward();
}
