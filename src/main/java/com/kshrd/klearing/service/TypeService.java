package com.kshrd.klearing.service;

import com.kshrd.klearing.model.request.TypeRequest;
import com.kshrd.klearing.model.response.TypeResponse;


import java.util.List;

public interface TypeService {

    List<TypeResponse> findAllType();

    List<TypeResponse> findTypeByName(String name);

    TypeResponse insertType(TypeRequest typeRequest);

    TypeResponse updateTypeByName(TypeResponse typeResponse);

    String deleteTypeByName(String typeName);
}
