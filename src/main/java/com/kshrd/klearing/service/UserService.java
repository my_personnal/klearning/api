package com.kshrd.klearing.service;


import com.kshrd.klearing.model.request.UpdateUserRequest;
import com.kshrd.klearing.model.request.UserRequest;
import com.kshrd.klearing.model.response.UserResponse;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UserService {

    List<UserResponse>getAllUsers();
    List<UserResponse> findUserByName(String name);

    String deleteUserById(Integer userId);

    UserResponse insertUser(UserRequest userRequest);

    UserResponse updateUserById(UpdateUserRequest updateUserRequest);


}
