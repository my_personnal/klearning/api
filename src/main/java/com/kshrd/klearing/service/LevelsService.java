package com.kshrd.klearing.service;
import com.kshrd.klearing.model.request.LevelsRequest;
import com.kshrd.klearing.model.request.UpdateLevelRequest;
import com.kshrd.klearing.model.response.LevelsResponse;

import java.util.List;

public interface LevelsService {

    List<LevelsResponse> findAllLevels();

    List<LevelsResponse> findLevelByName(String name);

    LevelsResponse insertLevel(LevelsRequest levelsRequest);

    LevelsResponse updateLevelByName(UpdateLevelRequest levelsResponse);

    Integer deleteLevelById(Integer id);
}
