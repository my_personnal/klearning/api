package com.kshrd.klearing.service;

import com.kshrd.klearing.model.AuthenticationUsers;
import com.kshrd.klearing.model.request.AuthUserRequestModel;
import com.kshrd.klearing.model.response.AuthUserResponseModel;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;


public interface AuthenticationUserService extends UserDetailsService {
    Boolean insertUser(AuthUserRequestModel user, int user_role);

    List<AuthUserResponseModel> findUserAll();

    AuthUserResponseModel findUserId(int id);

    Boolean updateUser(int id, AuthenticationUsers user);

    Boolean deleteUser(int id);

    List<String> getUsername();

    Boolean userStatus(String username);

    int selectUserID(String username);

}
