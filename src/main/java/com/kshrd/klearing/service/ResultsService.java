package com.kshrd.klearing.service;
import com.kshrd.klearing.model.result.ResultRequest;
import com.kshrd.klearing.model.result.ResultResponse;
import com.kshrd.klearing.model.result.ResultUpdateRequest;

import java.util.List;

public interface ResultsService {

    List<ResultResponse> findAllResults();

    List<ResultResponse> findLevelByName(Integer id);

    ResultResponse insertResult(ResultRequest resultRequest);

    ResultResponse updateResultByID(ResultUpdateRequest response);

    ResultResponse deleteResultsByID(Integer id);
}
